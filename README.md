# README #

Aplikacje demo SoftwareSkill dla modułu obsługi baz danych - część dotycząca Spring Data JPA.

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania API danego frameworka  
Moduły

* common-model - model (encje), wyjątki, konwerter wartości logicznych - używane w pozostałych modułach
* simple-spring-data-jpa - Aplikacja Spring Boot z REST Api oraz Hibernate jako implementacja JPA

* Wersja 1.0

### Opis ###

* Funkcjonalność - aplikacje w ramach modułów realizują tą samą kogikę, wyszukują w bazie danych karty o podanym
  identyfikatorze i wyświetlają jej dane. W przypadku gdy karta nie zostanie znaleziona wyświetlana jest informacja iż
  danych dla danego identyfikatora nie znaleziono.
* Baza danych - baza danych zawierająca informacje o kartach (identyfikator jako klucz, UUID karty, dane właściciela
  karty).
* Konfiguracja - nie ma konieczności konfiguracji. Wykorzystywana jest baza H2 w trybie in memory lub PostgreSQL.
* Zależności - H2 (baza danych in memory), Mockito, JUnit 5, Spring Boot, Logback (framework do logów aplikacyjnych),
* Endpointy Rest - klient np. Postman, aplikacja z anotacją @SpringBootApplication, kolekcja Postman w katalogu postman.
* Jak uruchomić testy - polecenie mvn test lub uruchomić z poziomu IDE (np. Run All tests w IntelliJ dla modułu).
* Jak uruchomić aplikację - z linii poleceń - zobacz pliki Application.

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka krzysztof.kadziolka@softwareskill.pl