package pl.softwareskill.course.springdata.simpleapp;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.notFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentDto;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentId;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z dostępem do departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    //Kod demonstracyjny, interfejs jest nadmiarowy (jedno użycie, jedna implementacja, nie jest to zasób 'zewnętrzny'),
    //wystarczy wstrzyknąć fasadę DepartmentFacade
    DepartmentReader departmentReader;

    @GetMapping
    ResponseEntity<DepartmentDto> findDepartment(@RequestParam Long id) {
        return departmentReader.findDepartment(new DepartmentId(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }
}
