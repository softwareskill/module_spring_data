package pl.softwareskill.course.springdata.simpleapp;

import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentDto;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentId;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
@Service
public class DepartmentFacade implements DepartmentReader {

    DepartmentRepository departmentRepository;

    @Override
    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentId departmentId) {
        log.info("Wyszukiwanie departamentu o identyfikatorze {}", departmentId);
        return departmentRepository.findById(departmentId.getValue())
                .map(Department::toDto);
    }
}
