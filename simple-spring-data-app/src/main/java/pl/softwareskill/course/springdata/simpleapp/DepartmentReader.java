package pl.softwareskill.course.springdata.simpleapp;

import java.util.Optional;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentDto;
import pl.softwareskill.course.springdata.simpleapp.dto.DepartmentId;

interface DepartmentReader {

    Optional<DepartmentDto> findDepartment(DepartmentId departmentId);
}
