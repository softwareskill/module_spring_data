package pl.softwareskill.course.springdata.simpleapp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//Anotacja @Repository jest nadmiarowa ale rekomendowane jest jej używanie - zamarkowanie repository
//oraz mogą działać jakieś aspekty, które szukają tej anotacji
@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

}
