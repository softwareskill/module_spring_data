package pl.softwareskill.course.springdata.simpleapp.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class AddressDto {

    String street;

    String streetNumber;

    String city;

    String country;

    String postalCode;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    @SuppressWarnings("unused")
    public AddressDto(String street, String streetNumber, String city, String country, String postalCode) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
        this.country = country;
        this.postalCode = postalCode;
    }
}
