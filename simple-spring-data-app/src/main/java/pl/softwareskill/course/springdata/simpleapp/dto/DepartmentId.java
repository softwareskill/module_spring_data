package pl.softwareskill.course.springdata.simpleapp.dto;

import lombok.NonNull;
import lombok.Value;

@Value
public class DepartmentId {

    @NonNull
    Long value;

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
