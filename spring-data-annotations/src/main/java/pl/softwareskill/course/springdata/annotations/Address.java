package pl.softwareskill.course.springdata.annotations;

import static java.util.Objects.isNull;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.annotations.dto.AddressDto;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
public class Address {

    @Column(name = "STREET")
    String street;

    @Column(name = "STREET_NUMBER")
    String streetNumber;

    @Column(name = "CITY")
    String city;

    @Column(name = "COUNTRY")
    String country;

    @Column(name = "POSTAL_CODE")
    String postalCode;

    public static Address fromDto(AddressDto addressDto) {
        if (isNull(addressDto) || addressDto.isEmpty()) {
            return null;
        }
        return Address.builder()
                .city(addressDto.getCity())
                .country(addressDto.getCountry())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .streetNumber(addressDto.getStreetNumber())
                .build();

    }
}
