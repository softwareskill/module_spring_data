package pl.softwareskill.course.springdata.annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class AnnotationsApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(AuditableDepartmentRepository departmentRepository) {
        return new DepartmentFacade(departmentRepository);
    }

    @Bean
    SecurityAuditorAware securityAuditorAware() {
        return new SecurityAuditorAware();
    }
}
