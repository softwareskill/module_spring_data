package pl.softwareskill.course.springdata.annotations;

import java.time.Instant;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.softwareskill.course.springdata.annotations.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.annotations.dto.UpdateDepartmentDto;

@Entity
@Table(name = "AUDITABLE_DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners({AuditingEntityListener.class, AuditableDepartmentEventListener.class})
@Slf4j
class AuditableDepartment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @CreatedBy
    String createdBy;

    @CreatedDate
    Instant createdAt;

    @LastModifiedBy
    String updatedBy;

    @LastModifiedDate
    Instant updatedAt;

    @Version
    Long version;

    @PostLoad
    private void postLoad() {
        log.info("Post load");
    }

    @PrePersist
    private void prePersist() {
        log.info("Pre persist");
    }

    @PostPersist
    private void postPersist() {
        log.info("Post persist");
    }

    @PreUpdate
    private void preUpdate() {
        log.info("Pre update");
    }

    @PostUpdate
    private void postUpdate() {
        log.info("Post update");
    }


    static AuditableDepartment fromDto(CreateDepartmentDto createDepartmentDto) {
        var department = new AuditableDepartment();
        department.setName(createDepartmentDto.getName());
        department.setAddress(Address.fromDto(createDepartmentDto.getAddress()));
        return department;
    }

    void apply(UpdateDepartmentDto updateDepartmentDto) {
        setName(updateDepartmentDto.getName());
        setAddress(Address.fromDto(updateDepartmentDto.getAddress()));
    }
}
