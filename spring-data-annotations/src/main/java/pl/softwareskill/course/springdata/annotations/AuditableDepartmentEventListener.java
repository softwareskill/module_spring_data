package pl.softwareskill.course.springdata.annotations;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class AuditableDepartmentEventListener {

    @PrePersist
    @PreUpdate
    @PreRemove
    private void beforeAnyUpdate(AuditableDepartment department) {
        log.info("Dodatkowy pre");
    }
}
