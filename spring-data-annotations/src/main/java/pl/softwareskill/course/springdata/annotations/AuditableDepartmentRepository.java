package pl.softwareskill.course.springdata.annotations;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditableDepartmentRepository extends JpaRepository<AuditableDepartment, Long> {

    boolean existsByNameIgnoreCase(String departmentName);
}
