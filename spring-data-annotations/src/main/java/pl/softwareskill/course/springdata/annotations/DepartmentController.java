package pl.softwareskill.course.springdata.annotations;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.annotations.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.annotations.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.annotations.dto.UpdateDepartmentDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z anotacjami org.springframework.data.*
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @PostMapping
    ResponseEntity<?> createDepartment(@RequestBody CreateDepartmentDto createDepartmentDto) {
        DepartmentCreationStatus creationStatus = facade.createDepartment(createDepartmentDto);
        switch (creationStatus) {
            case ALREADY_EXISTS:
                return ResponseEntity.status(HttpStatus.CONFLICT).body("{}");
            case INVALID_INPUT:
                return ResponseEntity.badRequest().body("{}");
            default:
                return ResponseEntity.ok().body("{}");
        }
    }

    @PutMapping("/{id}")
    ResponseEntity<Object> updateDepartment(@PathVariable("id") Long departmentId, @RequestBody UpdateDepartmentDto updateDepartmentDto) {
        if (facade.updateDepartment(departmentId, updateDepartmentDto)) {
            return ResponseEntity.ok().body("{}");
        }
        return ResponseEntity.notFound().build();
    }
}
