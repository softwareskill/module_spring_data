package pl.softwareskill.course.springdata.annotations;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.annotations.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.annotations.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.annotations.dto.UpdateDepartmentDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Ponieważ jest modyfikacja musi być Transactional oraz metody publiczne
public class DepartmentFacade {

    AuditableDepartmentRepository departmentRepository;

    @Transactional
    public DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto) {

        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        var validator = new DepartmentValidator(departmentRepository);
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        var department = AuditableDepartment.fromDto(createDepartmentDto);

        departmentRepository.save(department);

        return DepartmentCreationStatus.CREATED;
    }

    @Transactional
    public boolean updateDepartment(Long departmentId, UpdateDepartmentDto updateDepartmentDto) {

        return departmentRepository.findById(departmentId)
                .map(auditableDepartment -> {
                    auditableDepartment.setVersion(11L);//Wartość sterowana zewnętrznie - nie zostanie wzięta pod uwagę ta zmiana
                    auditableDepartment.apply(updateDepartmentDto);
                    return true;
                })
                .orElse(false);

    }
}
