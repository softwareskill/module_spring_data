package pl.softwareskill.course.springdata.annotations;

import static java.util.Objects.isNull;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.annotations.dto.CreateDepartmentDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class DepartmentValidator {

    AuditableDepartmentRepository repository;

    boolean isRequestValid(CreateDepartmentDto createDepartmentDto) {
        if (isNull(createDepartmentDto) || isNull(createDepartmentDto.getName())) {
            return false;
        }
        return departmentNameNotBlank(createDepartmentDto);
    }

    private static boolean departmentNameNotBlank(CreateDepartmentDto createDepartmentDto) {
        return !createDepartmentDto.getName().isBlank();
    }

    boolean departmentDoesntExist(String departmentName) {
        return !repository.existsByNameIgnoreCase(departmentName);
    }
}
