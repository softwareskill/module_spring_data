package pl.softwareskill.course.springdata.annotations;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.AuditorAware;

class SecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(UUID.randomUUID().toString());
    }
}
