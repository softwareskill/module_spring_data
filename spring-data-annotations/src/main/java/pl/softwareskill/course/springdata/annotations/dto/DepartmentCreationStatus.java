package pl.softwareskill.course.springdata.annotations.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    INVALID_INPUT
}
