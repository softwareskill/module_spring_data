package pl.softwareskill.course.springdata.annotations.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DepartmentName {

    @NonNull
    String value;

    @Override
    public String toString() {
        return value;
    }
}
