package pl.softwareskill.course.springdata.annotations;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import pl.softwareskill.course.springdata.annotations.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.annotations.dto.UpdateDepartmentDto;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = AnnotationsDemoApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AuditableDepartmentIntegrationTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    SampleDepartmentData sampleDepartmentData;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void shouldCreateAuditableDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .address(SampleDepartmentData.sampleAddress())
                .name(UUID.randomUUID().toString())
                .build();

        String contentAsJson = objectMapper.writeValueAsString(createDepartmentDto);
        mvc.perform(post("/departments")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdateAuditableDepartment() throws Exception {

        Long existingDepartment = 1L;

        UpdateDepartmentDto updateDepartmentDto = UpdateDepartmentDto.builder()
                .address(SampleDepartmentData.sampleAddress())
                .name(UUID.randomUUID().toString())
                .build();

        String contentAsJson = objectMapper.writeValueAsString(updateDepartmentDto);
        mvc.perform(put("/departments/{departmentId}", existingDepartment)
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
