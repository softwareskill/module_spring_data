INSERT INTO AUDITABLE_DEPARTMENTS (
    DEPARTMENT_ID,
    NAME,
    --Embedded address
    STREET,
    STREET_NUMBER,
    CITY,
    COUNTRY,
    POSTAL_CODE,
    CREATED_AT,
    CREATED_BY,
    UPDATED_AT,
    UPDATED_BY,
    VERSION
) VALUES (
    1,
    'Softwareskill',
    '1-go maja',
    '11A',
    'Katowice',
    'PL',
    '34-900',
    CURRENT_TIMESTAMP,
    'f4907f0d-a9d2-45f3-92df-70372a4d252f',
    null,
    null,
    1
);