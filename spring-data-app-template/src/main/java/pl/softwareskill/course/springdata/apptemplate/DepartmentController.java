package pl.softwareskill.course.springdata.apptemplate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.notFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.apptemplate.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentId;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentName;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z dostępem do departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    //Kod demonstracyjny - zamiast dwóch poniższych wystarczy wstrzyknąć DepartmentFacade
    DepartmentReader departmentReader;
    DepartmentCreator departmentCreator;

    @GetMapping
    ResponseEntity<DepartmentDto> findDepartment(@RequestParam Long id) {
        return departmentReader.findDepartment(new DepartmentId(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/{name}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable String name) {
        return departmentReader.findDepartment(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/search")
    ResponseEntity<DepartmentDto> findDepartment(@RequestParam(required = false) String name, @RequestParam(required = false) String city) {
        return departmentReader.findDepartmentByNameAndCity(new DepartmentName(name), city)
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @PutMapping
    ResponseEntity<?> createDepartment(@RequestBody CreateDepartmentDto createDepartmentDto) {
        DepartmentCreationStatus creationStatus = departmentCreator.createDepartment(createDepartmentDto);
        switch (creationStatus) {
            case ALREADY_EXISTS:
                return ResponseEntity.status(HttpStatus.CONFLICT).body("{}");
            case INVALID_INPUT:
                return ResponseEntity.badRequest().body("{}");
            default:
                return ResponseEntity.ok().body("{}");
        }
    }
}
