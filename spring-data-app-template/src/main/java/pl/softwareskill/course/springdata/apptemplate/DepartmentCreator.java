package pl.softwareskill.course.springdata.apptemplate;

import pl.softwareskill.course.springdata.apptemplate.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentCreationStatus;

interface DepartmentCreator {

    DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto);

}
