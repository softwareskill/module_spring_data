package pl.softwareskill.course.springdata.apptemplate;

import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.apptemplate.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentId;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentName;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Kod demonstracyjny DepartmentReader i DepartmentCreator są używane tylko w
//jednym kontrolerze i jest jedna implementacja
//Mozna je usunąć lub przerobić na klasy z fragmentami logiki z DepartmentFacade,
//a samą DepartmentFacade usunąć
public class DepartmentFacade implements DepartmentReader, DepartmentCreator {

    DepartmentRepository departmentRepository;

    @Override
    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentId departmentId) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentId);
        return departmentRepository.findById(departmentId.getValue())
                .map(Department::toDto);
    }

    @Override
    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findByNameIgnoreCase(departmentName.getValue())
                .map(Department::toDto);
    }

    @Override
    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public Optional<DepartmentDto> findDepartmentByNameAndCity(DepartmentName departmentName, String city) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findByNameAndAddress_CityAllIgnoreCase(departmentName.getValue(), city)
                .map(Department::toDto);
    }

    //Ponieważ jest zapis jest potrzebna transakcja i metoda musi być publiczna
    @Override
    @Transactional
    public DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto) {
        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        var validator = new DepartmentValidator(departmentRepository);
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        var department = Department.fromDto(createDepartmentDto);
        departmentRepository.save(department);
        log.info("Departament {} utworzony", departmentName);
        return DepartmentCreationStatus.CREATED;
    }
}
