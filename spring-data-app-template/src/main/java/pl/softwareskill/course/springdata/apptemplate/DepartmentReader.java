package pl.softwareskill.course.springdata.apptemplate;

import java.util.Optional;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentId;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentName;

interface DepartmentReader {

    Optional<DepartmentDto> findDepartment(DepartmentId departmentId);

    Optional<DepartmentDto> findDepartment(DepartmentName departmentName);

    Optional<DepartmentDto> findDepartmentByNameAndCity(DepartmentName departmentName, String city);
}
