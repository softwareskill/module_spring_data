package pl.softwareskill.course.springdata.apptemplate;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    boolean existsByNameIgnoreCase(String name);

    Optional<Department> findByNameIgnoreCase(String name);

    Optional<Department> findByNameAndAddress_CityAllIgnoreCase(String name, String city);
}
