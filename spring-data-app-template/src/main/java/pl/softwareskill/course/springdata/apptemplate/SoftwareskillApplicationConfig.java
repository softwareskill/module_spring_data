package pl.softwareskill.course.springdata.apptemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SoftwareskillApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository) {
        return new DepartmentFacade(departmentRepository);
    }
}
