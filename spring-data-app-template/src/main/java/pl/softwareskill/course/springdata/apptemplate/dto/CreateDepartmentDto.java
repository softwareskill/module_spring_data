package pl.softwareskill.course.springdata.apptemplate.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
public class CreateDepartmentDto {
    String name;

    AddressDto address;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    @SuppressWarnings("unused")
    public CreateDepartmentDto(String name, AddressDto address) {
        this.name = name;
        this.address = address;
    }
}
