package pl.softwareskill.course.springdata.apptemplate.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    INVALID_INPUT
}
