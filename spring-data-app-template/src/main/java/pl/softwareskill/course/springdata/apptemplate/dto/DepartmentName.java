package pl.softwareskill.course.springdata.apptemplate.dto;

import lombok.NonNull;
import lombok.Value;

@Value
public class DepartmentName {

    @NonNull
    String value;

    @Override
    public String toString() {
        return value;
    }
}
