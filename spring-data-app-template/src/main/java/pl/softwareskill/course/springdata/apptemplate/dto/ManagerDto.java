package pl.softwareskill.course.springdata.apptemplate.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class ManagerDto {
    String firstName;

    String lastName;
}
