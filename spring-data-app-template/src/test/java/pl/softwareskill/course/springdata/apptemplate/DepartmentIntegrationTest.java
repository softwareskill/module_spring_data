package pl.softwareskill.course.springdata.apptemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import pl.softwareskill.course.springdata.apptemplate.dto.AddressDto;
import pl.softwareskill.course.springdata.apptemplate.dto.CreateDepartmentDto;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SoftwareskillApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    DepartmentCreator departmentCreator;

    @Test
    void shouldFindDepartmentDataByName() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByNameAndCity() throws Exception {
        String existingDepartmentName = "Hibernate";
        String city = "Katowice";

        mvc.perform(get("/departments/search/?name={name}&city={city}", existingDepartmentName, city)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataById() throws Exception {
        long existingDepartmentId = 1L;

        mvc.perform(get("/departments?id=" + existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindCreatedDepartment() throws Exception {
        var existingDepartment = existingDepartment();

        AddressDto existingDepartmentAddress = existingDepartment.getAddress();
        mvc.perform(get("/departments/{name}", existingDepartment.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("$.name", equalTo(existingDepartment.getName())))
                //address
                .andExpect(jsonPath("$.address.street", equalTo(existingDepartmentAddress.getStreet())))
                .andExpect(jsonPath("$.address.streetNumber", equalTo(existingDepartmentAddress.getStreetNumber())))
                .andExpect(jsonPath("$.address.city", equalTo(existingDepartmentAddress.getCity())))
                .andExpect(jsonPath("$.address.country", equalTo(existingDepartmentAddress.getCountry())))
                .andExpect(jsonPath("$.address.postalCode", equalTo(existingDepartmentAddress.getPostalCode())));
    }

    private CreateDepartmentDto existingDepartment() {
        CreateDepartmentDto sampleDepartment = sampleCreateDepartmentDto();
        departmentCreator.createDepartment(sampleDepartment);
        return sampleDepartment;
    }

    private static CreateDepartmentDto sampleCreateDepartmentDto() {
        return CreateDepartmentDto.builder()
                .name("name" + System.currentTimeMillis())
                .address(sampleAddress())
                .build();
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }

    @Test
    void shouldNotFindNotExistingDepartment() throws Exception {
        Long notExistingDepartmentId = 100000L;

        mvc.perform(get(String.format("/departments/%d", notExistingDepartmentId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        //UWAGA REST API powinno być kompatybilne wstecznie
        //Nie jest to do końca prawidłowy przykład - używasz ObjectMappera - zmiana struktury danych od razu
        //zostanie naniesiona w testach, nie wykryjesz nieoczekiwanych zmian w strukturze danych wejściowych
        //(usunięcie pola, zmiana nazwy pola, przeniesienie pola).
        //
        //Prawidłowym podejściem jest użycie JSONa jako String i wstrzyknięcie wartości np. "{ "pole" : ${POLE} }"
        //Chyba że masz dodatkowe testy E2E/CDC, które zweryfikują starsze wersje requestów i odpowiedzi
        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        //Wywołanie logiki - 'przejdzie' przez mechanizmy deserializacji i serializacji
        mvc.perform(put("/departments")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    @Test
    void shouldNotCreateNonUniqueDepartment() throws Exception {
        String existingDepartmentName = "Hibernate";
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(put("/departments")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .build();
        mvc.perform(put("/departments")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
