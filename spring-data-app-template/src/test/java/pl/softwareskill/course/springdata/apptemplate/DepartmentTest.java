package pl.softwareskill.course.springdata.apptemplate;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import pl.softwareskill.course.springdata.apptemplate.dto.AddressDto;
import pl.softwareskill.course.springdata.apptemplate.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.apptemplate.dto.DepartmentName;

public class DepartmentTest {

    private DepartmentRepository departmentRepository;

    private DepartmentFacade departmentFacade;

    @BeforeEach
    void setup() {
        departmentRepository = Mockito.mock(DepartmentRepository.class);
        departmentFacade = new SoftwareskillApplicationConfig().departmentFacade(departmentRepository);
    }

    @Test
    void shouldFindDepartmentData() {
        //given existing department
        String existingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByNameIgnoreCase(eq(existingDepartmentName)))
                .thenReturn(Optional.of(Department.fromDto(sampleCreateDepartmentDto(existingDepartmentName))));

        var result = departmentFacade.findDepartment(new DepartmentName(existingDepartmentName));
        Assertions.assertTrue(result.isPresent());
    }

    @Test
    void shouldCreateDepartment() {
        var departmentToCreate = existingDepartment();

        var result = departmentFacade.createDepartment(departmentToCreate);
        Assertions.assertEquals(result, DepartmentCreationStatus.CREATED);
    }

    private CreateDepartmentDto existingDepartment() {
        CreateDepartmentDto sampleDepartment = sampleCreateDepartmentDto(sampleUniqueDepartmentName());
        departmentFacade.createDepartment(sampleDepartment);
        return sampleDepartment;
    }

    private static CreateDepartmentDto sampleCreateDepartmentDto(String name) {
        return CreateDepartmentDto.builder()
                .name(name)
                .address(sampleAddress())
                .build();
    }

    @Test
    void shouldNotFindNotExistingDepartment() {
        String notExistingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByNameIgnoreCase(eq(notExistingDepartmentName)))
                .thenReturn(Optional.empty());

        var result = departmentFacade.findDepartment(new DepartmentName(notExistingDepartmentName));

        Assertions.assertTrue(result.isEmpty());

    }

    @Test
    void shouldNotCreateNonUniqueDepartment() {
        String existingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.existsByNameIgnoreCase(eq(existingDepartmentName)))
                .thenReturn(true);

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        var result = departmentFacade.createDepartment(createDepartmentDto);
        Assertions.assertEquals(DepartmentCreationStatus.ALREADY_EXISTS, result);
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() {
        CreateDepartmentDto createDepartmentDto = invalidCreateDepartmentRequest();

        var result = departmentFacade.createDepartment(createDepartmentDto);
        Assertions.assertEquals(DepartmentCreationStatus.INVALID_INPUT, result);
    }

    private static CreateDepartmentDto invalidCreateDepartmentRequest() {
        return CreateDepartmentDto.builder().build();
    }

    private static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
