package pl.softwareskill.course.springdata.search;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
@EnableTransactionManagement
@EntityScan(basePackageClasses = Manager.class)
@EnableJpaRepositories
public class BasicSearchApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(BasicSearchApplication.class)
                .build();
        app.run(args);
    }
}
