package pl.softwareskill.course.springdata.search;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class BasicSearchApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentJpaRepository departmentRepository, DepartmentQbeExecutor departmentQbeExecutor) {
        return new DepartmentFacade(departmentRepository, departmentQbeExecutor);
    }
}
