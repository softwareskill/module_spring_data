package pl.softwareskill.course.springdata.search;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.notFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.search.dto.CountDto;
import pl.softwareskill.course.springdata.search.dto.DepartmentDto;
import pl.softwareskill.course.springdata.search.dto.DepartmentId;
import pl.softwareskill.course.springdata.search.dto.PageableSearchResultDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z dostępem do departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/basicsearch")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade departmentFacade;

    @GetMapping("/{id}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable Long id) {
        return departmentFacade.findDepartment(new DepartmentId(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping
    ResponseEntity<List<DepartmentDto>> findDepartmentsForCity(@RequestParam String city) {
        return ResponseEntity.ok(departmentFacade.findDepartmentsInCity(city));
    }

    @GetMapping("/count")
    ResponseEntity<CountDto> findDepartmentsCount(@RequestParam String name) {
        Long count = departmentFacade.findDepartmentsCountByName(name);
        return ResponseEntity.ok(new CountDto(count));
    }

    @GetMapping("/qbe")
    ResponseEntity<List<DepartmentDto>> findDepartmentsForCityQbe(@RequestParam String city, @RequestParam String departmentName) {
        return ResponseEntity.ok(departmentFacade.findDepartmentsInCityByExample(departmentName, city));
    }

    @GetMapping("/qbe/{name}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable String name) {
        return departmentFacade.findDepartmentByNameExample(name)
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/page")
    ResponseEntity<PageableSearchResultDto<DepartmentDto>> findDepartment(@RequestParam Integer pageNumber) {
        return ResponseEntity.ok(departmentFacade.findDepartmentsByPage(pageNumber));
    }

    @GetMapping("/pageNoNPlus1")
    ResponseEntity<PageableSearchResultDto<DepartmentDto>> findDepartmentNoNPlus1(@RequestParam Integer pageNumber) {
        return ResponseEntity.ok(departmentFacade.findDepartmentsByPageNoNPlusOne(pageNumber));
    }
}
