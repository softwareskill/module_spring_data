package pl.softwareskill.course.springdata.search;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.search.dto.DepartmentDto;
import pl.softwareskill.course.springdata.search.dto.DepartmentId;
import pl.softwareskill.course.springdata.search.dto.PageableSearchResultDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentJpaRepository departmentRepository;
    //Przykladowe QBE Repository (bez JpaRepository)
    DepartmentQbeExecutor departmentQbeExecutor;

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentId departmentId) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentId);
        return departmentRepository.findById(departmentId.getValue())
                .map(Department::toDto);
    }

    public Long findDepartmentsCountByName(String departmentName) {
        log.info("Wyszukiwanie departamentow o nazwie z {}", departmentName);
        return departmentRepository.countByNameLikeIgnoreCase(departmentName);
    }

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public List<DepartmentDto> findDepartmentsInCity(String city) {
        log.info("Wyszukiwanie departamentow dla miasta {}", city);
        return departmentRepository.findByAddress_CityLikeOrderByAddress_PostalCodeDesc(city)
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public List<DepartmentDto> findDepartmentsInCityByExample(String departmentName, String city) {
        log.info("Wyszukiwanie departamentu przez QBE dla miasta {} i nazwy {}", city, departmentName);
        return departmentRepository.findByNameEndingAndCityLikeExample(departmentName, city)
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public Optional<DepartmentDto> findDepartmentByNameExample(String name) {
        log.info("Wyszukiwanie departamentu przez QBE dla nazwy {}", name);
        return departmentRepository.findByNameExample(name)
                .map(Department::toDto);
    }

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public PageableSearchResultDto<DepartmentDto> findDepartmentsByPage(int pageNumber) {
        log.info("Stronicowanie wyszukiwanie departamentow - strona nr {}", pageNumber);
        Page<Department> departmentPage = departmentRepository.findPage(pageNumber);

        List<DepartmentDto> departments = getDepartmentDtos(departmentPage);

        return toPageableResult(departmentPage, departments);
    }

    //Jeżeli dociąganie będzie LAZY i nie będzie @Transactional i public to zostanie rzucony błąd na doczytaniu Lazy
    @Transactional
    public PageableSearchResultDto<DepartmentDto> findDepartmentsByPageNoNPlusOne(int pageNumber) {
        log.info("Stronicowanie wyszukiwanie departamentow bez N+1- strona nr {}", pageNumber);

        var pageableRequest = PageRequest.of(pageNumber, 2);//, Sort.by("name").ascending());

        Page<Department> departmentPage = departmentRepository.findAllPageable(pageableRequest);

        List<DepartmentDto> departments = getDepartmentDtos(departmentPage);

        return toPageableResult(departmentPage, departments);
    }

    private PageableSearchResultDto<DepartmentDto> toPageableResult(Page<Department> departmentPage, List<DepartmentDto> departments) {
        return PageableSearchResultDto.<DepartmentDto>builder()
                .items(departments)
                .numberOfElements(departmentPage.getNumberOfElements())
                .pageNumber(departmentPage.getNumber())
                .totalPages(departmentPage.getTotalPages())
                .totalElements(departmentPage.getTotalElements())
                .build();
    }

    private List<DepartmentDto> getDepartmentDtos(Page<Department> departmentPage) {
        return departmentPage
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }
}
