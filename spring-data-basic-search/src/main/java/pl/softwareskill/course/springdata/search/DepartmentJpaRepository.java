package pl.softwareskill.course.springdata.search;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.endsWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentJpaRepository extends JpaRepository<Department, Long> {

    Long countByNameLikeIgnoreCase(String name);

    List<Department> findByAddress_CityLikeOrderByAddress_PostalCodeDesc(String name);

    default Optional<Department> findByNameExample(String name) {
        Department department = new Department();
        department.setName(name);

        Example<Department> example = Example.of(department);

        return findOne(example);
    }

    default List<Department> findByNameEndingAndCityLikeExample(String name, String city) {
        Department department = new Department();
        department.setName(name);
        department.setAddress(Address.builder()
                .city(city)
                .build());

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", endsWith().ignoreCase())
                .withMatcher("address.city", contains().ignoreCase());

        Example<Department> example = Example.of(department, matcher);

        Sort sort = Sort.by("address.city").ascending()
                .and(Sort.by("address.postalCode").descending());

        return findAll(example, sort);
    }

    default Page<Department> findPage(int pageNumber) {
        return findAll(PageRequest.of(pageNumber, 2, Sort.by("name").ascending()));
    }

    @Query(value = "FROM Department d left join fetch d.manager", countQuery = "select count(*) from Department")
    Page<Department> findAllPageable(Pageable pageable);
}
