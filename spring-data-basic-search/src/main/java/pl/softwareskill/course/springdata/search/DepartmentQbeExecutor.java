package pl.softwareskill.course.springdata.search;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentQbeExecutor extends CrudRepository<Department, Long>, QueryByExampleExecutor<Department> {
}
