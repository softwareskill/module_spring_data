package pl.softwareskill.course.springdata.search;

import static java.util.Objects.isNull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.search.dto.ManagerDto;

@Entity
@Table(name = "MANAGERS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "department")
class Manager {
    @Id
    @Column(name = "MANAGER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MANAGERS")
    Long managerId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    @OneToOne(mappedBy = "manager", fetch = FetchType.EAGER)
    Department department;

    public static ManagerDto toDto(Manager manager) {
        if (isNull(manager)) {
            return null;
        }
        return ManagerDto.builder()
                //jeżeli użyjesz pola dla LAZY będą nulle jeśli obiekt nie został doczytany
                .firstName(manager.getFirstName())
                .lastName(manager.getLastName())
                .build();
    }
}
