package pl.softwareskill.course.springdata.search;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = BasicSearchApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindDepartmentDataById() throws Exception {
        Long existingDepartmentId = 1L;

        mvc.perform(get("/departments/basicsearch/{id}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentsCountByName() throws Exception {
        String name = "Hiber%";

        mvc.perform(get("/departments/basicsearch/count?name={name}", name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.count", anyNumberMatcher()));
    }

    @Test
    void shouldFindDepartmentsInCity() throws Exception {
        String city = "Katowic%";

        mvc.perform(get("/departments/basicsearch?city={city}", city)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentsByCityQbe() throws Exception {
        String city = "Katowic";
        String departmentName = "bernate";

        mvc.perform(get("/departments/basicsearch/qbe?city={city}&departmentName={departmentName}", city, departmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentByNameQbe() throws Exception {
        String name = "Hibernate";

        mvc.perform(get("/departments/basicsearch/qbe/{name}", name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentsPage() throws Exception {
        int pageNumber = 0;

        mvc.perform(get("/departments/basicsearch/page?pageNumber={pageNumber}", pageNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", anyNumberMatcher()))
                .andExpect(jsonPath("$.totalPages", anyNumberMatcher()))
                .andExpect(jsonPath("$.numberOfElements", anyNumberMatcher()))
                .andExpect(jsonPath("$.pageNumber", anyNumberMatcher()))
                .andExpect(jsonPath("$.items[0].name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentsPageNoNPlus1() throws Exception {
        int pageNumber = 0;

        mvc.perform(get("/departments/basicsearch/pageNoNPlus1?pageNumber={pageNumber}", pageNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", anyNumberMatcher()))
                .andExpect(jsonPath("$.totalPages", anyNumberMatcher()))
                .andExpect(jsonPath("$.numberOfElements", anyNumberMatcher()))
                .andExpect(jsonPath("$.pageNumber", anyNumberMatcher()))
                .andExpect(jsonPath("$.items[0].name", anyStringMatcher()));
    }

    private static Matcher<Number> anyNumberMatcher() {
        return CoreMatchers.any(Number.class);
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }

    @Test
    void shouldNotFindNotExistingDepartment() throws Exception {
        Long notExistingDepartmentId = 100000L;

        mvc.perform(get("/departments/basicsearch/{id}", notExistingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
