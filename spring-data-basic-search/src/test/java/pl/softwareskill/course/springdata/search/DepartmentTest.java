package pl.softwareskill.course.springdata.search;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import pl.softwareskill.course.springdata.search.dto.AddressDto;
import pl.softwareskill.course.springdata.search.dto.DepartmentId;

public class DepartmentTest {

    private DepartmentJpaRepository departmentRepository;
    private DepartmentQbeExecutor departmentQbeExecutor;

    private DepartmentFacade departmentFacade;

    @BeforeEach
    void setup() {
        departmentRepository = Mockito.mock(DepartmentJpaRepository.class);
        departmentQbeExecutor = Mockito.mock(DepartmentQbeExecutor.class);
        departmentFacade = new BasicSearchApplicationConfig().departmentFacade(departmentRepository, departmentQbeExecutor);
    }

    @Test
    void shouldFindDepartmentData() {
        //given existing department
        Long existingDepartmentId = 1L;

        Mockito.when(departmentRepository.findById(eq(existingDepartmentId)))
                .thenReturn(Optional.of(sampleDepartment(existingDepartmentId)));

        var result = departmentFacade.findDepartment(new DepartmentId(existingDepartmentId));
        Assertions.assertTrue(result.isPresent());
    }

    private static Department sampleDepartment(Long id) {
        Department department = new Department();
        department.setName(sampleUniqueDepartmentName());
        department.setDepartmentId(id);
        department.setAddress(Address.fromDto(sampleAddress()));
        return department;
    }

    @Test
    void shouldNotFindNotExistingDepartment() {
        Long notExistingDepartmentId = -1L;

        Mockito.when(departmentRepository.findById(eq(notExistingDepartmentId)))
                .thenReturn(Optional.empty());

        var result = departmentFacade.findDepartment(new DepartmentId(notExistingDepartmentId));

        Assertions.assertTrue(result.isEmpty());

    }

    private static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
