package pl.softwareskill.course.springdata.dml;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.dml.dto.CreateDepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "manager")
@NamedQueries(
        {
                @NamedQuery(name = "clearManager", query = "update Department d set d.manager = null where d.manager.id = :managerId"),
                @NamedQuery(name = "removeDepartment", query = "delete from Department d where d.id = :departmentId")
        }
)
class Department {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANAGER_ID")
    Manager manager;

    static Department fromDto(CreateDepartmentDto createDepartmentDto) {
        var department = new Department();
        department.setName(createDepartmentDto.getName());
        department.setAddress(Address.fromDto(createDepartmentDto.getAddress()));
        return department;
    }

    void clearManager() {
        manager.setDepartment(null);
        manager = null;
    }
}
