package pl.softwareskill.course.springdata.dml;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.DepartmentName;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z usuwaniem departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentDeleteController {

    DepartmentDeleteFacade facade;

    @DeleteMapping("/byEntity/{departmentId}")
    ResponseEntity<Object> deleteUsingEntity(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithEntity(new DepartmentId(departmentId));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/byQuery/{departmentId}")
    ResponseEntity<Object> deleteUsingQuery(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithQuery(new DepartmentId(departmentId));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/byCriteria/{departmentId}")
    ResponseEntity<Object> deleteUsingCriteria(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithCriteria(new DepartmentId(departmentId));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/byName/{name}")
    ResponseEntity<Object> deleteByName(@PathVariable("name") String name) {
        facade.deleteByName(new DepartmentName(name));
        return ResponseEntity.ok().build();
    }
}
