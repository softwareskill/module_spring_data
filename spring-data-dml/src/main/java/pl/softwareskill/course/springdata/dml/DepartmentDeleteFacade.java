package pl.softwareskill.course.springdata.dml;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.DepartmentName;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Ponieważ jest modyfikacja musi być Transactional oraz metody publiczne
public class DepartmentDeleteFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;

    @Transactional
    public void deleteWithQuery(DepartmentId departmentId) {

        //Anotacja Query w Repository
        Long departmentIdValue = departmentId.getValue();
        departmentRepository.deleteByQueryAnnotation(departmentIdValue);

        //Query z wykorzystaniem EntityManager
        var query = entityManager.createQuery("delete from Department d where d.departmentId = :departmentId");
        query.setParameter("departmentId", departmentIdValue);
        var deletedDepartmentsCount = query.executeUpdate();
        log.info("Usunięto {} departamentow", deletedDepartmentsCount);

        //Query z wykorzystaniem named query zdefiniowanym w encji Departmnet
        var namedQuery = entityManager.createNamedQuery("removeDepartment");
        namedQuery.setParameter("departmentId", departmentIdValue);
        var deletedDepartmentsCount1 = namedQuery.executeUpdate();
        log.info("Usunięto {} departamentow", deletedDepartmentsCount1);
    }

    @Transactional
    public void deleteWithEntity(DepartmentId departmentId) {
        departmentRepository.findById(departmentId.getValue())
                .ifPresent(departmentRepository::delete);
    }

    @Transactional
    public void deleteWithCriteria(DepartmentId departmentId) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();

        CriteriaDelete<Department> delete = cb.createCriteriaDelete(Department.class);

        var root = delete.from(Department.class);

        //alias id NIE jest dostepny w CriteriaAPI
        delete.where(cb.equal(root.get(Department_.DEPARTMENT_ID), departmentId.getValue()));
        Query deleteQuery = this.entityManager.createQuery(delete);
        var deletedCount = deleteQuery.executeUpdate();

        log.info("Usunieto {} departamentow", deletedCount);
    }

    @Transactional
    public void deleteByName(DepartmentName departmentName) {
        departmentRepository.deleteByName(departmentName.getValue());
    }
}
