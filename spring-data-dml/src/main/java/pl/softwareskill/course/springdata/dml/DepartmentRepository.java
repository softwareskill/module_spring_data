package pl.softwareskill.course.springdata.dml;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    boolean existsByNameIgnoreCase(String name);

    //Bez anotacji @Modyfying zostanie rzucony wyjątek
    @Modifying
    //alias id jest dostepny dla JPA - reprezentuje klucz główny encji
    @Query("update Department d set d.manager = null where d.manager.id = :managerId")
    void unlinkManager(Long managerId);

    //Bez anotacji @Modyfying zostanie rzucony wyjątek
    @Modifying
    //alias id jest dostepny dla JPA - reprezentuje klucz główny encji
    @Query("delete from Department d where d.id = :departmentId")
    void deleteByQueryAnnotation(Long departmentId);

    void deleteByName(String name);
}
