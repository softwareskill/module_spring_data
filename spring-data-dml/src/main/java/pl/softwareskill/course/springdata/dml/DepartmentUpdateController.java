package pl.softwareskill.course.springdata.dml;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.ManagerId;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z edycją departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentUpdateController {

    DepartmentUpdateFacade facade;

    @PutMapping("/{departmentId}/unlinkManagerEntity")
    ResponseEntity<Object> unlinkManagerUsingEntity(@PathVariable("departmentId") Long departmentId) {
        facade.unlinkManagerWithEntity(new DepartmentId(departmentId));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/unlinkManagerQuery/{managerId}")
    ResponseEntity<Object> unlinkManagerUsingQuery(@PathVariable("managerId") Long managerId) {
        facade.unlinkManagerWithQuery(new ManagerId(managerId));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/unlinkManagerCriteria/{managerId}")
    ResponseEntity<Object> unlinkManagerUsingCriteria(@PathVariable("managerId") Long managerId) {
        facade.unlinkManagerWithCriteria(new ManagerId(managerId));
        return ResponseEntity.ok().build();
    }
}
