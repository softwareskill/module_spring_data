package pl.softwareskill.course.springdata.dml;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.ManagerId;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Ponieważ jest modyfikacja musi być Transactional oraz metody publiczne
public class DepartmentUpdateFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;

    @Transactional
    public void unlinkManagerWithQuery(ManagerId managerId) {

        //Anotacja Query w Repository
        Long managerIdValue = managerId.getValue();
        departmentRepository.unlinkManager(managerIdValue);

        //Query z wykorzystaniem EntityManager
        var query = entityManager.createQuery("update Department d set d.manager = null where d.manager.id = :managerId");
        query.setParameter("managerId", managerIdValue);
        var updatedDepartmentsCount = query.executeUpdate();
        log.info("Odpieto managera {} z {} departamentow", managerIdValue, updatedDepartmentsCount);

        //Query z wykorzystaniem named query zdefiniowanym w encji Departmnet
        var namedQuery = entityManager.createNamedQuery("clearManager");
        namedQuery.setParameter("managerId", managerIdValue);
        var updatedDepartmentsCount1 = namedQuery.executeUpdate();
        log.info("Odpieto managera {} z {} departamentow", managerIdValue, updatedDepartmentsCount1);
    }

    @Transactional
    public void unlinkManagerWithEntity(DepartmentId departmentId) {
        departmentRepository.findById(departmentId.getValue())
                .ifPresent(Department::clearManager);
    }

    @Transactional
    public void unlinkManagerWithCriteria(ManagerId managerId) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();

        CriteriaUpdate<Department> update = cb.createCriteriaUpdate(Department.class);

        var root = update.from(Department.class);

        //Używamy wygenerowanych klas (target/generates-sources) - tzw JPA Metadata
        update.set(Department_.MANAGER, null);
        //alias id NIE jest dostepny w CriteriaAPI
        update.where(cb.equal(root.get(Department_.MANAGER).get(Manager_.MANAGER_ID), managerId.getValue()));

        Query updateQuery = this.entityManager.createQuery(update);
        var updatedCount = updateQuery.executeUpdate();

        log.info("Odpieto managera {} z {} departamentow", managerId.getValue(), updatedCount);
    }
}
