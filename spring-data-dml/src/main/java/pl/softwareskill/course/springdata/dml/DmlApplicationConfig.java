package pl.softwareskill.course.springdata.dml;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DmlApplicationConfig {

    @Bean
    DepartmentUpdateFacade departmentUpdateFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentUpdateFacade(departmentRepository, entityManager);
    }

    @Bean
    DepartmentCreatorFacade departmentCreatorFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentCreatorFacade(departmentRepository, entityManager);
    }

    @Bean
    DepartmentDeleteFacade departmentDeleteFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentDeleteFacade(departmentRepository, entityManager);
    }
}
