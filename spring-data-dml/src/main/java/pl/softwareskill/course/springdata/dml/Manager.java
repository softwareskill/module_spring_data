package pl.softwareskill.course.springdata.dml;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "MANAGERS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "department")
class Manager {
    @Id
    @Column(name = "MANAGER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MANAGERS")
    Long managerId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    @OneToOne(mappedBy = "manager", fetch = FetchType.LAZY)
    Department department;
}
