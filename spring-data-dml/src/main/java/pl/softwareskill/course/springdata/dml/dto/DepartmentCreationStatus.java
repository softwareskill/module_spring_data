package pl.softwareskill.course.springdata.dml.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    INVALID_INPUT
}
