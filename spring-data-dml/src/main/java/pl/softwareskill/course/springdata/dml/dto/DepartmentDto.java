package pl.softwareskill.course.springdata.dml.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import lombok.Builder;
import lombok.Value;

@Value
//Zamień @Value na poniższe i zobacz jak wzrośnie CodeCoverage dla klasy DepartmentDto
//@Getter
//@RequiredArgsConstructor
//@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class DepartmentDto {
    String name;

    AddressDto address;

    ManagerDto manager;
}
