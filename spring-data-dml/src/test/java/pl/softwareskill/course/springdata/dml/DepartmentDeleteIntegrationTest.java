package pl.softwareskill.course.springdata.dml;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.DepartmentName;

public class DepartmentDeleteIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldDeleteDepartmentWithEntity() throws Exception {
        Long existingDepartmentId = existingDepartmentId().getValue();

        mvc.perform(delete("/departments/byEntity/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    void shouldDeleteDepartmentWithRepository() throws Exception {
        Long existingDepartmentId = existingDepartmentId().getValue();

        mvc.perform(delete("/departments/byQuery/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteDepartmentWithCriteria() throws Exception {
        Long existingDepartmentId = existingDepartmentId().getValue();

        mvc.perform(delete("/departments/byCriteria/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteDepartmentByName() throws Exception {
        String existingDepartmentName = existingDepartmentName();

        mvc.perform(delete("/departments/byName/{departmentName}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String existingDepartmentName() {
        String existingDepartmentName = "Hibernate" + System.nanoTime();
        sampleDepartmentData.createSampleDepartment(new DepartmentName(existingDepartmentName));
        return existingDepartmentName;
    }

    private DepartmentId existingDepartmentId() {
        return sampleDepartmentData.createSampleDepartment();
    }
}
