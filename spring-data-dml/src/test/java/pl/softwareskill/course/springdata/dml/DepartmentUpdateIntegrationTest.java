package pl.softwareskill.course.springdata.dml;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DepartmentUpdateIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldUnlinkManagerWithEntity() throws Exception {
        Long existingDepartmentId = 1L;

        mvc.perform(put("/departments/{departmentId}/unlinkManagerEntity", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUnlinkManagerWithRepository() throws Exception {
        Long existingManagerId = 1L;

        mvc.perform(put("/departments/unlinkManagerQuery/{managerId}", existingManagerId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUnlinkManagerWithCriteria() throws Exception {
        Long existingManagerId = 1L;

        mvc.perform(put("/departments/unlinkManagerCriteria/{managerId}", existingManagerId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
