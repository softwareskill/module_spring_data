package pl.softwareskill.course.springdata.dml;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.dml.dto.AddressDto;
import pl.softwareskill.course.springdata.dml.dto.DepartmentId;
import pl.softwareskill.course.springdata.dml.dto.DepartmentName;

@Component
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Transactional
class SampleDepartmentData {

    DepartmentRepository repository;

    public DepartmentId createSampleDepartment() {
        return createAndSaveDepartment("Test" + System.nanoTime());
    }

    public DepartmentId createSampleDepartment(DepartmentName departmentName) {
        return createAndSaveDepartment(departmentName.getValue());
    }

    private DepartmentId createAndSaveDepartment(String departmentName) {
        var department = new Department();
        department.setName(departmentName);
        department.setAddress(Address.fromDto(sampleAddress()));
        department = repository.save(department);
        return new DepartmentId(department.getDepartmentId());
    }


    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
