package pl.softwareskill.course.springdata.join;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.notFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.join.dto.DepartmentDto;
import pl.softwareskill.course.springdata.join.dto.DepartmentId;
import pl.softwareskill.course.springdata.join.dto.DepartmentName;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej JOIN
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/joins")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @GetMapping("/byId/{id}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable Long id) {
        return facade.findDepartment(new DepartmentId(id))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/inner/{name}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable String name) {
        return facade.findDepartment(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/outer/{name}")
    ResponseEntity<DepartmentDto> findDepartmentJoin(@PathVariable String name) {
        return facade.findDepartmentJoin(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/joinFetch/{name}")
    ResponseEntity<DepartmentDto> findDepartmentJoinFetch(@PathVariable String name) {
        return facade.findDepartmentJoinFetch(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/joinFetchCriteria/{name}")
    ResponseEntity<DepartmentDto> findDepartmentJoinFetchCriteria(@PathVariable String name) {
        return facade.findDepartmentJoinFetchCriteria(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/inner/manyEntities/{name}")
    ResponseEntity<DepartmentDto> findDepartmentWithMantEntities(@PathVariable String name) {
        return facade.findDepartmentWithManyEntities(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }
}
