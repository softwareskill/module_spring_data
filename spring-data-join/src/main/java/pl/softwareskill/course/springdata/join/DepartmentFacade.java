package pl.softwareskill.course.springdata.join;

import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.join.dto.DepartmentDto;
import pl.softwareskill.course.springdata.join.dto.DepartmentId;
import pl.softwareskill.course.springdata.join.dto.DepartmentName;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;

    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentId departmentId) {
        log.info("Wyszukiwanie departamentu o id {}", departmentId);
        //Bazowa metoda z CrudRepository
        return departmentRepository.findById(departmentId.getValue())
                .map(Department::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findByName(departmentName.getValue())
                .map(Department::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentJoin(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findByNameJoin(departmentName.getValue())
                .map(Department::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentJoinFetch(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findByNameJoinFetch(departmentName.getValue())
                .map(Department::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentJoinFetchCriteria(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Department> q = cb.createQuery(Department.class);

        Root<Department> r = q.from(Department.class);

        r.fetch("manager", JoinType.LEFT);
        //Zamien fetch na join i zobacz jakie zapytanai się wygenerują
        //r.join("manager", JoinType.LEFT);

        q.where(cb.equal(r.get("name"), departmentName.getValue()));

        return Optional.of(entityManager.createQuery(q))
                .map(TypedQuery::getSingleResult)
                .map(Department::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentWithManyEntities(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        var resultArray = departmentRepository.findByNameWithManyEntities(departmentName.getValue());
        if (resultArray.isEmpty()) {
            return Optional.empty();
        }
        //Nieoptymalne rozwiązanie
        var department = (Department) resultArray.get(0)[0];
        //Zwracany wynik to tablica [Department,Manager]
        return Optional.of(department.toDto());
    }
}
