package pl.softwareskill.course.springdata.join;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    //Sprawdz dla relacji LAZY i EAGER
    @Query("Select d from Department d INNER JOIN d.manager where d.name=:name")
    Optional<Department> findByName(String name);

    //Sprawdz dla relacji LAZY i EAGER
    //Sprawdz dla relacji wymaganej i opcjonalnej
    //Sprawdz dla JOIN, LEFT JOIN, RIGHT JOIN
    @Query("Select d from Department d JOIN d.manager where d.name=:name")
    Optional<Department> findByNameJoin(String name);

    //Sprawdz dla relacji LAZY i EAGER
    //Sprawdz dla JOIN FETCH, INNER JOIN FETCH, LEFT JOIN FETCH, RIGHT JOIN FETCH
    @Query("Select d from Department d LEFT JOIN FETCH d.manager where d.name=:name")
    Optional<Department> findByNameJoinFetch(String name);

    //Sprawdz dla relacji LAZY i EAGER
    //Nieoptymalne rowiązanie lepiej użyć JOIN FETCH
    @Query("Select d, m from Department d INNER JOIN d.manager m where d.name=:name")
    List<Object[]> findByNameWithManyEntities(String name);

}
