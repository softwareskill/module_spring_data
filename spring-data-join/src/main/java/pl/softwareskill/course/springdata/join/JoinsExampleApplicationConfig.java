package pl.softwareskill.course.springdata.join;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class JoinsExampleApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentFacade(departmentRepository, entityManager);
    }
}
