package pl.softwareskill.course.springdata.join;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = JoinsExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindDepartmentDataById() throws Exception {
        Long existingDepartmentId = 1L;

        mvc.perform(get("/departments/joins/byId/{id}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByName() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/joins/inner/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByNameWithManyEntitiesQuery() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/joins/inner/manyEntities/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByNameWithOuter() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/joins/outer/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByNameWithJoinFetch() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/joins/joinFetch/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDataByNameWithJoinFetchCriteria() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/joins/joinFetchCriteria/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()));
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }

}
