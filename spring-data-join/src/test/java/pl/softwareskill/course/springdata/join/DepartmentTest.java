package pl.softwareskill.course.springdata.join;

import java.util.Optional;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import pl.softwareskill.course.springdata.join.dto.AddressDto;
import pl.softwareskill.course.springdata.join.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.join.dto.DepartmentName;

public class DepartmentTest {

    private DepartmentRepository departmentRepository;

    private DepartmentFacade departmentFacade;

    @BeforeEach
    void setup() {
        departmentRepository = Mockito.mock(DepartmentRepository.class);
        departmentFacade = new JoinsExampleApplicationConfig().departmentFacade(departmentRepository, Mockito.mock(EntityManager.class));
    }

    @Test
    void shouldFindDepartmentData() {
        //given existing department
        String existingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByName(eq(existingDepartmentName)))
                .thenReturn(Optional.of(Department.fromDto(sampleCreateDepartmentDto(existingDepartmentName))));

        var result = departmentFacade.findDepartment(new DepartmentName(existingDepartmentName));
        Assertions.assertTrue(result.isPresent());
    }

    private static CreateDepartmentDto sampleCreateDepartmentDto(String name) {
        return CreateDepartmentDto.builder()
                .name(name)
                .address(sampleAddress())
                .build();
    }

    @Test
    void shouldNotFindNotExistingDepartment() {
        String notExistingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByName(eq(notExistingDepartmentName)))
                .thenReturn(Optional.empty());

        var result = departmentFacade.findDepartment(new DepartmentName(notExistingDepartmentName));

        Assertions.assertTrue(result.isEmpty());

    }


    private static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
