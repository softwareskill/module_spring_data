package pl.softwareskill.course.springdata.mapping;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DataMappingApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentFacade(departmentRepository, entityManager);
    }
}
