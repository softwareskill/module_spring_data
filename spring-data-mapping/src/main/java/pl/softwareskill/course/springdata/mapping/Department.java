package pl.softwareskill.course.springdata.mapping;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "manager")
class Department {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MANAGER_ID")
    Manager manager;

    DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(getName())
                .address(Address.toDto(getAddress()))
                .build();
    }
}
