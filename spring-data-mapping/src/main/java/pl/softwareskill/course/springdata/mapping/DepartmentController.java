package pl.softwareskill.course.springdata.mapping;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.notFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentDto;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentName;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentsPerCityDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z dostępem do departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @GetMapping("/{name}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable String name) {
        return facade.findDepartment(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/dto/{name}")
    ResponseEntity<DepartmentDto> findDepartmentDto(@PathVariable String name) {
        return facade.findDepartmentDto(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/dtoByClass/{name}")
    ResponseEntity<DepartmentDto> findDepartmentDtoByClass(@PathVariable String name) {
        return facade.findDepartmentDto_ByClass(new DepartmentName(name))
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/byCities")
    ResponseEntity<List<DepartmentsPerCityDto>> findDepartmentsByCity() {
        return ResponseEntity.ok(facade.getDepartmentPerCity());
    }

    @GetMapping("/byCitiesClassConstructor")
    ResponseEntity<List<DepartmentsPerCityDto>> findDepartmentsByCity_ByClassConstructor() {
        return ResponseEntity.ok(facade.getDepartmentPerCity_ByClassConstructor());
    }

    @GetMapping("/byCitiesTuple")
    ResponseEntity<List<DepartmentsPerCityDto>> findDepartmentsByCity_ByTuple() {
        return ResponseEntity.ok(facade.getDepartmentPerCity_ByTuple());
    }

    @GetMapping("/byCitiesTupleEM")
    ResponseEntity<List<DepartmentsPerCityDto>> findDepartmentsByCity_ByTupleEM() {
        return ResponseEntity.ok(facade.getDepartmentPerCityWithTupleAndEM());
    }
}
