package pl.softwareskill.course.springdata.mapping;

import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentDto;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentName;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentProjection;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentsPerCityDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentRepository departmentRepository;

    EntityManager entityManager;

    @Transactional
    public Optional<DepartmentDto> findDepartment(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        //Zostaną wygenerowane dwa zapytania - jedno właściwe a drugie dla id encji
        Optional<DepartmentView> optionalDepartment = departmentRepository.findByNameIgnoreCase(departmentName.getValue());

        optionalDepartment.ifPresent(departmentView -> {
            log.info("Imie i nazwisko managera {}", departmentView.getManager().getFullName());
            //sprawdzamy czy pojawi się select dla identyfikatora - jeśli nie to encja jest w PC
            departmentRepository.findById(departmentView.getDepartmentId());
        });

        return optionalDepartment.map(DepartmentView::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentDto(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findDtoByNameAllIgnoreCase(departmentName.getValue())
                .map(DepartmentProjection::toDto);
    }

    @Transactional
    public Optional<DepartmentDto> findDepartmentDto_ByClass(DepartmentName departmentName) {
        log.info("Wyszukiwanie departamentu o nazwie {}", departmentName);
        return departmentRepository.findDtoByNameAllIgnoreCase(departmentName.getValue(), DepartmentProjection.class)
                .map(DepartmentProjection::toDto);
    }

    public List<DepartmentsPerCityDto> getDepartmentPerCity() {
        log.info("Zestawienie departamanetow dla miasta");
        return departmentRepository.departmentsByCity()
                .stream()
                .map(DepartmentsPerCityView::toDto)
                .collect(toList());
    }

    public List<DepartmentsPerCityDto> getDepartmentPerCityWithTupleAndEM() {
        log.info("Zestawienie departamanetow dla miasta");

        var query = entityManager.createQuery(
                "SELECT d.address.city as cityName, COUNT(d.address.city) as totalCount "
                        + "FROM Department AS d GROUP BY d.address.city ORDER BY d.address.city DESC", Tuple.class);

        return query.getResultList()
                .stream()
                .map(tuple -> DepartmentsPerCityDto.builder()
                        .cityName(tuple.get("cityName", String.class))
                        .totalCount(tuple.get("totalCount", Long.class))
                        .build()
                )
                .collect(toList());
    }

    public List<DepartmentsPerCityDto> getDepartmentPerCity_ByClassConstructor() {
        log.info("Zestawienie departamanetow dla miasta");
        return departmentRepository.departmentsByCity_ByClassConstructor()
                .stream()
                .map(DepartmentsPerCity::toDto)
                .collect(toList());
    }

    public List<DepartmentsPerCityDto> getDepartmentPerCity_ByTuple() {
        log.info("Zestawienie departamanetow dla miasta");
        return departmentRepository.departmentsByCity_ByTuple()
                .stream()
                .map(tuple -> DepartmentsPerCityDto.builder()
                        .cityName(tuple.get("cityName", String.class))
                        .totalCount(tuple.get("totalCount", Long.class))
                        .build()
                )
                .collect(toList());
    }
}
