package pl.softwareskill.course.springdata.mapping;

import java.util.List;
import java.util.Optional;
import javax.persistence.Tuple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentProjection;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    //Mapping projection by interface
    Optional<DepartmentView> findByNameIgnoreCase(String name);

    //Mapping projection by DTO
    Optional<DepartmentProjection> findDtoByNameAllIgnoreCase(String name);

    //Dynamic Mapping projection
    <T> Optional<T> findDtoByNameAllIgnoreCase(String name, Class<T> clazz);

    //Aggregate by interface
    @Query(value = "SELECT d.city AS cityName, COUNT(d.*) AS totalCount "
            + "FROM departments AS d GROUP BY d.city ORDER BY d.city DESC", nativeQuery = true)
    List<DepartmentsPerCityView> departmentsByCity();

    //Aggregate by Class constructor
    @Query("SELECT new pl.softwareskill.course.springdata.mapping.DepartmentsPerCity(d.address.city, COUNT(d.address.city)) "
            + "FROM Department AS d GROUP BY d.address.city ORDER BY d.address.city DESC")
    List<DepartmentsPerCity> departmentsByCity_ByClassConstructor();

    //Tuple
    @Query("SELECT d.address.city as cityName, COUNT(d.address.city) as totalCount "
            + "FROM Department AS d GROUP BY d.address.city ORDER BY d.address.city DESC")
    List<Tuple> departmentsByCity_ByTuple();
}
