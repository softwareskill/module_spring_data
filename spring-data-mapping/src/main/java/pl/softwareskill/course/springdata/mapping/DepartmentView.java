package pl.softwareskill.course.springdata.mapping;

import javax.persistence.Embedded;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentDto;

public interface DepartmentView {

    Long getDepartmentId();

    String getName();

    @Embedded
    Address getAddress();

    ManagerView getManager();

    default DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(getName())
                .address(Address.toDto(getAddress()))
                .manager(ManagerView.toDto(getManager()))
                .build();
    }
}
