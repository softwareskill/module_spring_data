package pl.softwareskill.course.springdata.mapping;

import static java.util.Objects.isNull;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentsPerCityDto;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DepartmentsPerCity {

    String cityName;

    Long totalCount;

    public DepartmentsPerCity(String cityName, Long totalCount) {
        this.cityName = cityName;
        this.totalCount = totalCount;
    }

    static DepartmentsPerCityDto toDto(DepartmentsPerCity departmentsPerCity) {
        if (isNull(departmentsPerCity)) {
            return null;
        }
        return DepartmentsPerCityDto.builder()
                .cityName(departmentsPerCity.cityName)
                .totalCount(departmentsPerCity.totalCount)
                .build();
    }
}
