package pl.softwareskill.course.springdata.mapping;

import static java.util.Objects.isNull;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentsPerCityDto;

public interface DepartmentsPerCityView {

    String getCityName();

    Long getTotalCount();

    static DepartmentsPerCityDto toDto(DepartmentsPerCityView departmentsPerCity) {
        if (isNull(departmentsPerCity)) {
            return null;
        }
        return DepartmentsPerCityDto.builder()
                .cityName(departmentsPerCity.getCityName())
                .totalCount(departmentsPerCity.getTotalCount())
                .build();
    }
}
