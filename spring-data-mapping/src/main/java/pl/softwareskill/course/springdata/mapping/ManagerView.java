package pl.softwareskill.course.springdata.mapping;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Value;
import pl.softwareskill.course.springdata.mapping.dto.ManagerDto;

public interface ManagerView {

    String getFirstName();

    String getLastName();

    //Open projection
    @Value("#{target.firstName + ' ' + target.lastName}")
    String getFullName();

    static ManagerDto toDto(ManagerView managerView) {
        if (Objects.isNull(managerView)) {
            return null;
        }
        return ManagerDto.builder()
                .firstName(managerView.getFirstName())
                .lastName(managerView.getLastName())
                .build();
    }
}
