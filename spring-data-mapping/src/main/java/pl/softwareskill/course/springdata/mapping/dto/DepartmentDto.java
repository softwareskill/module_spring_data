package pl.softwareskill.course.springdata.mapping.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class DepartmentDto {
    String name;

    AddressDto address;

    ManagerDto manager;
}
