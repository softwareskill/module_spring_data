package pl.softwareskill.course.springdata.mapping.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.mapping.Address;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DepartmentProjection {
    String name;

    AddressDto address;

    /**
     * Parametry konstruktora z atrybutów encji.
     * <p>
     * Dla projekcji parametr address nie może być typu AddresDto gdyż wymagane są typy jak pola w encji
     *
     * @param name    nazwa
     * @param address adres
     */
    public DepartmentProjection(String name, Address address) {
        this.name = name;
        this.address = Address.toDto(address);
    }

    public DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(getName())
                .address(address)
                .build();
    }
}
