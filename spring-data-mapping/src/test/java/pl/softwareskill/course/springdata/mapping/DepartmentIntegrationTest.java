package pl.softwareskill.course.springdata.mapping;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DataMappingApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindDepartmentDataByName() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()))
                //manager
                .andExpect(jsonPath("$.manager.firstName", anyStringMatcher()))
                .andExpect(jsonPath("$.manager.lastName", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDtoByName() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/dto/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentDtoByNameAndClass() throws Exception {
        String existingDepartmentName = "Hibernate";

        mvc.perform(get("/departments/dtoByClass/{name}", existingDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", anyStringMatcher()))
                //address
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.streetNumber", anyStringMatcher()))
                .andExpect(jsonPath("$.address.street", anyStringMatcher()))
                .andExpect(jsonPath("$.address.city", anyStringMatcher()))
                .andExpect(jsonPath("$.address.country", anyStringMatcher()))
                .andExpect(jsonPath("$.address.postalCode", anyStringMatcher()));
    }

    @Test
    void shouldFindDepartmentsByCity() throws Exception {
        mvc.perform(get("/departments/byCities")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].cityName", anyStringMatcher()))
                .andExpect(jsonPath("$.[0].totalCount", anyNumberMatcher()));
    }

    @Test
    void shouldFindDepartmentsByCity_ByClassConstructor() throws Exception {
        mvc.perform(get("/departments/byCitiesClassConstructor")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].cityName", anyStringMatcher()))
                .andExpect(jsonPath("$.[0].totalCount", anyNumberMatcher()));
    }

    @Test
    void shouldFindDepartmentsByCity_ByTuple() throws Exception {
        mvc.perform(get("/departments/byCitiesTuple")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].cityName", anyStringMatcher()))
                .andExpect(jsonPath("$.[0].totalCount", anyNumberMatcher()));
    }

    @Test
    void shouldFindDepartmentsByCity_ByTupleEM() throws Exception {
        mvc.perform(get("/departments/byCitiesTupleEM")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].cityName", anyStringMatcher()))
                .andExpect(jsonPath("$.[0].totalCount", anyNumberMatcher()));
    }

    private static Matcher<Number> anyNumberMatcher() {
        return CoreMatchers.any(Number.class);
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }
}
