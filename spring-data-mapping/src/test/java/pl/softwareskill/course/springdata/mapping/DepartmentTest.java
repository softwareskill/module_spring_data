package pl.softwareskill.course.springdata.mapping;

import java.util.Optional;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import pl.softwareskill.course.springdata.mapping.dto.DepartmentName;

public class DepartmentTest {

    private DepartmentRepository departmentRepository;

    private DepartmentFacade departmentFacade;
    private EntityManager entityManager;

    @BeforeEach
    void setup() {
        departmentRepository = Mockito.mock(DepartmentRepository.class);
        entityManager = Mockito.mock(EntityManager.class);
        departmentFacade = new DataMappingApplicationConfig().departmentFacade(departmentRepository, entityManager);
    }

    @Test
    void shouldFindDepartmentData() {
        //given existing department
        String existingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByNameIgnoreCase(eq(existingDepartmentName)))
                .thenReturn(Optional.of(sampleDepartmentView()));

        var result = departmentFacade.findDepartment(new DepartmentName(existingDepartmentName));
        Assertions.assertTrue(result.isPresent());
    }

    private static DepartmentView sampleDepartmentView() {
        return new DepartmentView() {

            @Override
            public Long getDepartmentId() {
                return System.nanoTime();
            }

            @Override
            public ManagerView getManager() {
                return sampleManagerView();
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public Address getAddress() {
                return null;
            }
        };
    }

    private static ManagerView sampleManagerView() {
        return new ManagerView() {
            @Override
            public String getFullName() {
                return null;
            }

            @Override
            public String getFirstName() {
                return null;
            }

            @Override
            public String getLastName() {
                return null;
            }
        };
    }

    @Test
    void shouldNotFindNotExistingDepartment() {
        String notExistingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(departmentRepository.findByNameIgnoreCase(eq(notExistingDepartmentName)))
                .thenReturn(Optional.empty());

        var result = departmentFacade.findDepartment(new DepartmentName(notExistingDepartmentName));

        Assertions.assertTrue(result.isEmpty());

    }

    private static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }
}
