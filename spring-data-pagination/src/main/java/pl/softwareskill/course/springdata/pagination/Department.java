package pl.softwareskill.course.springdata.pagination;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.pagination.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.pagination.dto.DepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "manager")
class Department {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "MANAGER_ID")
    Manager manager;

    DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(name)
                .address(Address.toDto(address))
                .manager(Manager.toDto(manager))
                .build();
    }

    static Department fromDto(CreateDepartmentDto createDepartmentDto) {
        var department = new Department();
        department.setName(createDepartmentDto.getName());
        department.setAddress(Address.fromDto(createDepartmentDto.getAddress()));
        return department;
    }
}
