package pl.softwareskill.course.springdata.pagination;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.pagination.dto.DepartmentDto;
import pl.softwareskill.course.springdata.pagination.dto.DepartmentName;
import pl.softwareskill.course.springdata.pagination.dto.PageableSearchResultDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej sortowaniem i paginacją
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/pagination")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @GetMapping("/jpa/sortedByMethodName")
    List<DepartmentDto> findAllSortedByMethodName(@RequestParam("name") String name) {
        return facade.findByNameSortedWithMethodName(name);
    }

    @GetMapping("/jpa/sorted")
    List<DepartmentDto> findAllSorted() {
        return facade.findAllSorted();
    }

    @GetMapping("/jpa/pageable")
    List<DepartmentDto> findAllPageable() {
        return facade.findAllPageable();
    }

    @GetMapping("/jpa/sorted/byName")
    List<DepartmentDto> findByNameLikeSorted(@RequestParam("name") String name) {
        return facade.findByNameLike(new DepartmentName(name));
    }

    @GetMapping("/jpa/pageable/byNames")
    PageableSearchResultDto<DepartmentDto> findAllSortedByName(@RequestParam("name") List<String> names, @RequestParam("pageNum") int pageNum) {
        return facade.findPageByNameIn(names, pageNum);
    }

    @GetMapping("/jpa/pageable/byNamesWithEM")
    PageableSearchResultDto<DepartmentDto> findAllSortedByNameWithEntityManager(@RequestParam("name") List<String> names, @RequestParam("pageNum") int pageNum) {
        return facade.findByNameInWithEntityManager(names, pageNum);
    }
}
