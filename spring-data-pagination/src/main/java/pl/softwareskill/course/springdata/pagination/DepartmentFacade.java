package pl.softwareskill.course.springdata.pagination;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.pagination.dto.DepartmentDto;
import pl.softwareskill.course.springdata.pagination.dto.DepartmentName;
import pl.softwareskill.course.springdata.pagination.dto.PageableSearchResultDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;

    @Transactional
    public List<DepartmentDto> findByNameLike(DepartmentName departmentName) {

        Sort sort = Sort.by(Sort.Order.asc("name"));//Można użyć metamodelu Department_.NAME

        return departmentRepository.findByNameLike(departmentName.getValue(), sort)
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<DepartmentDto> findAllPageable() {

        var pageable = PageRequest.of(0, 2, Sort.by("name").descending());

        var departmentIterable = departmentRepository.findAll(pageable);

        return StreamSupport.stream(departmentIterable.spliterator(), false)
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<DepartmentDto> findAllSorted() {

        var sort = Sort.by("name").and(Sort.by("manager.lastName").descending()).descending();

        var departmentIterable = departmentRepository.findAll(sort);

        return StreamSupport.stream(departmentIterable.spliterator(), false)
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<DepartmentDto> findByNameSortedWithMethodName(String name) {

        return departmentRepository.findAllByNameLikeOrderByNameAscManager_LastNameAsc(name)
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public PageableSearchResultDto<DepartmentDto> findPageByNameIn(List<String> names, int pageNum) {

        final int pageSize = 2;

        var pageable = PageRequest.of(pageNum, pageSize, Sort.by("name").descending());

        //Zobacz co sie stanie jak order w Pageable jest inny niz w query?
        //var pageable = PageRequest.of(0, 2, Sort.by("manager.lastName").ascending());
        var resultPage = departmentRepository.findByNameIn(names, pageable);

        var resultItems = resultPage.stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
        return toPageableSearchResult(pageNum, resultPage.getTotalElements(), pageSize, resultItems);
    }

    @Transactional
    public PageableSearchResultDto<DepartmentDto> findByNameInWithEntityManager(List<String> names, int pageNum) {

        final int pageSize = 2;

        //Wyszukanie liczby elementów - wymagane osobne zapytanie - TAKI SAM WARUNEK WYSZUKIWANIA
        Query queryTotalItemsCount = entityManager.createQuery("Select count(d.id) from Department d where name in (:names)");
        queryTotalItemsCount.setParameter("names", names);
        long totalItemsCountResult = (long) queryTotalItemsCount.getSingleResult();

        //Właściwe zapytanie
        var query = entityManager.createQuery("from Department where name in (:names)", Department.class);
        query.setParameter("names", names);
        query.setMaxResults(pageSize); //rozmiar strony
        query.setFirstResult(pageNum * pageSize); //mumer strony - 0 pierwsza strona

        var pageItemsList = query.getResultList()
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
        return toPageableSearchResult(pageNum, totalItemsCountResult, pageSize, pageItemsList);
    }

    private PageableSearchResultDto<DepartmentDto> toPageableSearchResult(int pageNum, long totalItemsCountResult, int pageSize, List<DepartmentDto> pageItemsList) {
        return PageableSearchResultDto.<DepartmentDto>builder()
                .items(pageItemsList)
                .pageNumber(pageNum)
                .totalElements(totalItemsCountResult)
                .totalPages(calculatePagesCount(totalItemsCountResult, pageSize))
                .numberOfElements(pageItemsList.size())
                .build();
    }

    private int calculatePagesCount(long totalItemsCountResult, int pageSize) {
        return BigDecimal.valueOf(totalItemsCountResult, 2)
                .add(BigDecimal.valueOf((double) 0.5))//dodajemy 0.5
                .divide(BigDecimal.valueOf(pageSize, 2))
                .intValue();
    }
}
