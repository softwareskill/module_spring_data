package pl.softwareskill.course.springdata.pagination;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;

public interface DepartmentRepository extends Repository<Department, Long>, PagingAndSortingRepository<Department, Long> {

    List<Department> findAllByNameLikeOrderByNameAscManager_LastNameAsc(String name);

    List<Department> findByNameLike(String name, Sort sort);

    //Zobacz co sie stanie jak order w Pageable jest inny niz w query?
    @Query("select d from Department d where d.name in (:names) order by name desc")
    Page<Department> findByNameIn(List<String> names, Pageable pageable);
}
