package pl.softwareskill.course.springdata.pagination;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class PaginationExampleApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentFacade(departmentRepository, entityManager);
    }
}
