package pl.softwareskill.course.springdata.pagination.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    INVALID_INPUT
}
