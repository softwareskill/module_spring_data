package pl.softwareskill.course.springdata.pagination.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DepartmentId {

    @NonNull
    Long value;

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
