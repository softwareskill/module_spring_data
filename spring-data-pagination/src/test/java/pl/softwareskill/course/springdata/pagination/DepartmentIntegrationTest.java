package pl.softwareskill.course.springdata.pagination;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = PaginationExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindSortedDepartmentsDataByNameWithMethodName() throws Exception {
        String existingDepartmentPrefix = "%bernat%";

        mvc.perform(get("/departments/pagination/jpa/sortedByMethodName?name={name}", existingDepartmentPrefix)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }


    @Test
    void shouldReturnSortedAllDepartments() throws Exception {

        mvc.perform(get("/departments/pagination/jpa/sorted")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }

    @Test
    void shouldReturnPagedAndSortedAllDepartments() throws Exception {

        mvc.perform(get("/departments/pagination/jpa/pageable")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }

    @Test
    void shouldFindSortedDepartmentsDataByName() throws Exception {
        String existingDepartmentPrefix = "%bernat%";

        mvc.perform(get("/departments/pagination/jpa/sorted/byName?name={name}", existingDepartmentPrefix)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }

    @Test
    void shouldSearchSortedDepartmentsDataByName() throws Exception {
        String existingDepartmentPrefix = "%bernat%";

        mvc.perform(get("/departments/pagination/jpa/sorted/byName?name={name}", existingDepartmentPrefix)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }

    @Test
    void shouldSearchFirstPagePageableDepartmentsDataByNames() throws Exception {
        final String existingDepartmentName = "Hibernate";
        final String existingDepartmentName1 = "Maven";
        final int existingPageNumber = 0;

        mvc.perform(get("/departments/pagination/jpa/pageable/byNames?name={name}&name={name1}&pageNum={pageNum}",
                existingDepartmentName, existingDepartmentName1, existingPageNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.totalPages", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.numberOfElements", CoreMatchers.equalTo(2)))
                .andExpect(jsonPath("$.pageNumber", CoreMatchers.equalTo(existingPageNumber)))
                .andExpect(jsonPath("$.items[0].address.city", anyStringMatcher()));
    }

    @Test
    void shouldSearchNotExistingPagePageableDepartmentsDataByNames() throws Exception {
        final String existingDepartmentName = "Hibernate";
        final String existingDepartmentName1 = "Maven";
        final int notExistingPageNumber = 10;

        mvc.perform(get("/departments/pagination/jpa/pageable/byNames?name={name},name={name1}&pageNum={pageNum}",
                existingDepartmentName, existingDepartmentName1, notExistingPageNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.totalPages", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.numberOfElements", CoreMatchers.equalTo(0)))
                .andExpect(jsonPath("$.pageNumber", CoreMatchers.equalTo(notExistingPageNumber)))
                .andExpect(jsonPath("$.items").isArray())
                .andExpect(jsonPath("$.items").isEmpty());
    }

    @Test
    void shouldSearchFirstPageOfDepartmentsDataByNamesWithEM() throws Exception {
        final String existingDepartmentName = "Hibernate";
        final String existingDepartmentName1 = "Maven";
        final int existingPageNumber = 0;

        mvc.perform(get("/departments/pagination/jpa/pageable/byNamesWithEM?name={name}&name={name1}&pageNum={pageNum}",
                existingDepartmentName, existingDepartmentName1, existingPageNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.totalPages", CoreMatchers.any(Number.class)))
                .andExpect(jsonPath("$.numberOfElements", CoreMatchers.equalTo(2)))
                .andExpect(jsonPath("$.pageNumber", CoreMatchers.equalTo(existingPageNumber)))
                .andExpect(jsonPath("$.items[0].address.city", anyStringMatcher()));
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }
}
