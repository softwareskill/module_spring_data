package pl.softwareskill.course.springdata.query;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@DiscriminatorValue("bike")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class Bike extends Vehicle {

    @Column(name = "FRONT_COUNT")
    int frontCount;

    @Column(name = "REAR_COUNT")
    int rearCount;
}
