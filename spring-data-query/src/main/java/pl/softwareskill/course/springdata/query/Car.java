package pl.softwareskill.course.springdata.query;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.query.dto.CarDto;

@Entity
@DiscriminatorValue("car")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class Car extends Vehicle {

    @Column(name = "GEAR_COUNT")
    int gearCount;

    @Column(name = "CAR_BRAND")
    String carBrand;

    public CarDto toDto() {
        return CarDto.builder()
                .cardBrand(getCarBrand())
                .gearCount(getGearCount())
                .build();
    }
}
