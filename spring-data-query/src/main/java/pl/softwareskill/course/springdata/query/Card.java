package pl.softwareskill.course.springdata.query;

import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.query.dto.CardDto;

@Entity
@Table(name = "Cards")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card {
    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    @ManyToOne
    @JoinColumn(name = "CARD_OWNER_ID")
    CardOwner owner;

    @Column(name = "EXPIRES_AT")
    Instant expiresAt;

    BigDecimal balance;

    public static CardDto toDto(Card card) {
        return CardDto.builder()
                .balance(card.getBalance())
                .cardCountry(card.getCardCountry())
                .expiresAt(card.getExpiresAt())
                .cardUuid(card.getCardUuid())
                .owner(CardOwner.toDto(card.getOwner()))
                .build();
    }
}
