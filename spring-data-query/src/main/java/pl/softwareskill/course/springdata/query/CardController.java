package pl.softwareskill.course.springdata.query;

import java.math.BigDecimal;
import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.query.dto.CardDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL i JPQL
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/cards/queries")
@SuppressWarnings("unused")
public class CardController {

    CardFacade facade;

    @GetMapping("/hql")
    ResponseEntity<Integer> findExpiringCardsCountForYearHQL(@RequestParam("year") Integer year) {
        return ResponseEntity.ok(facade.findExpiringCardsCountForYearHQL(year));
    }

    @GetMapping("/jpql")
    ResponseEntity<Integer> findExpiringCardsCountForYearJPQL(@RequestParam("year") Integer year) {
        return ResponseEntity.ok(facade.findExpiringCardsCountForYearJPQL(year));
    }

    @GetMapping("/criteria/allVIPCards")
    ResponseEntity<List<CardDto>> findAllVIPCards() {
        return ResponseEntity.ok(facade.findAllVIPCards());
    }

    @GetMapping("/criteria/findAll")
    ResponseEntity<List<CardDto>> findAll() {
        return ResponseEntity.ok(facade.findAll());
    }

    @GetMapping("/criteria/expiringInAYearWithBalance")
    ResponseEntity<List<CardDto>> findExpiringInAYearWithBalance(@RequestParam("minBalance") BigDecimal minBalance,
                                                                 @RequestParam("maxBalance") BigDecimal maxBalance) {
        return ResponseEntity.ok(facade.findExpiringInAYearWithBalance(minBalance, maxBalance));
    }
}
