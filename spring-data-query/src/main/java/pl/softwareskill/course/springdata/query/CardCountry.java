package pl.softwareskill.course.springdata.query;

public enum CardCountry {
    PL,
    EN,
    DE
}
