package pl.softwareskill.course.springdata.query;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.query.dto.CardDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class CardFacade {

    EntityManager entityManager;

    //Bez @Transactional nie zadziała
    @Transactional
    public Integer findExpiringCardsCountForYearHQL(int expiresInYear) {

        //Pobranie sesji Hibernate z EntityManager
        var session = entityManager.unwrap(Session.class);

        //Funkcja YEAR dostępna dla HQL
        var hql = "from Card c where YEAR(c.expiresAt) > :year";
        Query<Card> query = session.createQuery(hql, Card.class);
        query.setParameter("year", expiresInYear);

        //Nie zadziała dla samego znaku ? jako parametru (tak jak w JDBC)
//        var hql = "from Card c where YEAR(c.expiresAt) > ?";
//        Query<Department> query = session.createQuery(hql);
//        query.setParameter(1, expiresInYear);

        //Dla ?1 zadziała, wielokrotnie można użyć ?1
//        var hql = "from Card c where YEAR(c.expiresAt) > ?1 AND MONTH(c.expiresAt) > ?1";
//        Query<Department> query = session.createQuery(hql);
//        query.setParameter(1, expiresInYear);


        return query.getResultList().size();
    }

    //Korzystając z EntityManager dla odczytu nie musi być @transactional
    public Integer findExpiringCardsCountForYearJPQL(int expiresInYear) {

        //Funkcja YEAR dostępna dla HQL zadziała także w JPQL jeśli implementacja JPA to Hibernate
        var jpql = "select count(distinct c.id) from Card c where YEAR(c.expiresAt) > :year";
        var query = entityManager.createQuery(jpql, Long.class);
        query.setParameter("year", expiresInYear);

        return query.getSingleResult().intValue();
    }

    public List<CardDto> findAllVIPCards() {

        var jpql = "from Card where balance > 12000";
        var query = entityManager.createQuery(jpql, Card.class);

        return query.getResultList()
                .stream()
                .map(Card::toDto)
                .collect(Collectors.toList());
    }

    public List<CardDto> findAll() {

        var builer = entityManager.getCriteriaBuilder();

        var criteriaQuery = builer.createQuery(Card.class);

        var root = criteriaQuery.from(Card.class);

        //Bez fetch będzie problem N+1
        root.fetch("owner", JoinType.LEFT);

        criteriaQuery.select(root);

        var query = entityManager.createQuery(criteriaQuery);

        return query.getResultList()
                .stream()
                .map(Card::toDto)
                .collect(Collectors.toList());
    }

    public List<CardDto> findExpiringInAYearWithBalance(BigDecimal minBalance, BigDecimal maxBalance) {

        var builer = entityManager.getCriteriaBuilder();

        var criteriaQuery = builer.createQuery(Card.class);

        var root = criteriaQuery.from(Card.class);

//        Predicate balancePredicate = builer.between(root.get("balance"), minBalance, maxBalance);

        //Odkomentuj dla sprawdzenia zapytania z parametrami
        var minParam = builer.parameter(BigDecimal.class, "min");
        var maxParam = builer.parameter(BigDecimal.class, "max");
        Predicate balancePredicate = builer.between(root.get("balance"), minParam, maxParam);

        Predicate expiresAtPredicate = builer.greaterThan(root.get("expiresAt"), Instant.now().minus(365, ChronoUnit.DAYS));

        criteriaQuery.where(builer.and(balancePredicate, expiresAtPredicate));

        criteriaQuery.orderBy(builer.asc(root.get("cardCountry")), builer.desc(root.get("cardUuid")));

        criteriaQuery.distinct(true);

        //Bez fetch będzie problem N+1
        root.fetch("owner", JoinType.LEFT);

        criteriaQuery.select(root);

        var query = entityManager.createQuery(criteriaQuery);

        //Odkomentuj dla sprawdzenia zapytania z parametrami
        query.setParameter("min", minBalance);
        query.setParameter("max", maxBalance);

        return query.getResultList()
                .stream()
                .map(Card::toDto)
                .collect(Collectors.toList());
    }
}
