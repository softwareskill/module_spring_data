package pl.softwareskill.course.springdata.query;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.query.dto.UserDto;

@Entity
@Table(name = "USERS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class CardOwner {
    @Id
    @Column(name = "USER_ID")
    String userId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    public static UserDto toDto(CardOwner cardOwner) {
        return UserDto.builder()
                .firstName(cardOwner.getFirstName())
                .lastName(cardOwner.getLastName())
                .build();
    }
}
