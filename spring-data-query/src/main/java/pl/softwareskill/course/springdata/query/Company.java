package pl.softwareskill.course.springdata.query;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.query.dto.CompanyDto;

@Entity
@Table(name = "COMPANIES")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class Company {
    @Id
    @Column(name = "COMPANY_ID")
    Long companyId;

    String name;

    @OneToMany
    @JoinTable(name = "EMPLOYEE_COMPANY", joinColumns = @JoinColumn(name = "COMPANY_ID"), inverseJoinColumns = @JoinColumn(name = "EMPLOYEE_ID"))
    List<Employee> employees;

    public static CompanyDto toDto(Company company) {
        return CompanyDto.builder()
                .name(company.getName())
                .companyId(company.getCompanyId())
                .build();
    }
}
