package pl.softwareskill.course.springdata.query;

import pl.softwareskill.course.springdata.query.dto.AvgEmployeeAgeForCompanyDto;

public interface CompanyAvgAge {

    String getName();

    Long getAvgAge();

    default AvgEmployeeAgeForCompanyDto toDto() {
        return AvgEmployeeAgeForCompanyDto.builder()
                .name(getName())
                .avgAge(getAvgAge())
                .build();
    }
}
