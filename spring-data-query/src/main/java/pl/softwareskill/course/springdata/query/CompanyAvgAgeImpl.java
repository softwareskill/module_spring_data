package pl.softwareskill.course.springdata.query;

import java.math.BigDecimal;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CompanyAvgAgeImpl implements CompanyAvgAge {

    String name;
    Long avgAge;

    public CompanyAvgAgeImpl(String name, double avgAge) {
        this.name = name;
        //Dodatkowe przemapowanie typów aby wiek byl jako liczba stałoprzecinkowa (prosta konwersja)
        this.avgAge = BigDecimal.valueOf(avgAge).longValue();
    }
}
