package pl.softwareskill.course.springdata.query;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.query.dto.AvgEmployeeAgeForCompanyDto;
import pl.softwareskill.course.springdata.query.dto.CompanyDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL, JPQL i CriteriaAPI
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/companies")
@SuppressWarnings("unused")
public class CompanyController {

    CompanyFacade facade;

    @GetMapping("/companiesForEmployee")
    List<CompanyDto> findCompaniesForEmployee(@RequestParam("lastName") String lastName, @RequestParam("firstName") String firstName) {
        return facade.findCompaniesForEmployee(lastName, firstName);
    }

    @GetMapping("/avgEmployeeAge")
    List<AvgEmployeeAgeForCompanyDto> avgEmployeeAge() {
        return facade.avgEmployeesAgeForCompanies();
    }

    @GetMapping("/avgEmployeeAgeWithEM")
    List<AvgEmployeeAgeForCompanyDto> avgEmployeeAgeWithEM() {
        return facade.avgEmployeesAgeForCompaniesWithEM();
    }

    @GetMapping("/last")
    ResponseEntity<CompanyDto> findLastCompany() {
        return facade.findLastCompany()
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/lastWithEM")
    ResponseEntity<CompanyDto> findLastCompanyWithEM() {
        return facade.findLastCompanyWithEM()
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/withManyEmployees")
    ResponseEntity<List<CompanyDto>> findCompaniesWithManyEmployees() {
        return ResponseEntity.ok(facade.findCompaniesWithManyEmployees());
    }
}
