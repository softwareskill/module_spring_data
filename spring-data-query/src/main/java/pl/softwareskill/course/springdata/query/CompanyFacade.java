package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.springdata.query.dto.AvgEmployeeAgeForCompanyDto;
import pl.softwareskill.course.springdata.query.dto.CompanyDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class CompanyFacade {

    CompanyRepository repository;
    EntityManager entityManager;

    public List<CompanyDto> findCompaniesForEmployee(String lastName, String firstName) {
        return repository.findCompaniesForEmployee(lastName, firstName)
                .stream()
                .map(Company::toDto)
                .collect(Collectors.toList());
    }

    public List<AvgEmployeeAgeForCompanyDto> avgEmployeesAgeForCompanies() {
        return repository.findCompanyAvgAge()
                .stream()
                .map(CompanyAvgAge::toDto)
                .collect(Collectors.toList());
    }

    public List<AvgEmployeeAgeForCompanyDto> avgEmployeesAgeForCompaniesWithEM() {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        //Nie zadziała z interfejsem
        var cq = cb.createQuery(CompanyAvgAgeImpl.class);
        var root = cq.from(Company.class);

        //Relacja
        var companyEmployeeSetJoin = root.joinList("employees");

        //Podzapytanie SELECT AVG(e2.age) FROM Employees e2
        //Double bo AVG
        var sub = cq.subquery(Double.class);
        var subRoot = sub.from(Employee.class);
        //AVG(age)
        sub.select(cb.avg(subRoot.get("age")));

        // Fraza HAVING AVG({RELACJA}.age) > {PODZAPYTANIE}
        // HAVING AVG(e.age) > (SELECT AVG(e2.age) FROM Employee e2)
        cq.having(cb.gt(cb.avg(companyEmployeeSetJoin.get("age")), sub));

        //Kolejność i typy zwracanych wartośc zgodne z konstruktorem CompanyAvgAgeImpl
        cq.multiselect(root.get("name"), cb.avg(companyEmployeeSetJoin.get("age")));
        //Grupowanie
        cq.groupBy(root.get("name"));

        var query = entityManager.createQuery(cq);
        return query.getResultList()
                .stream()
                .map(CompanyAvgAge::toDto)
                .collect(Collectors.toList());
    }

    public Optional<CompanyDto> findLastCompany() {
        return repository.findLastCompany()
                .map(Company::toDto);
    }

    public Optional<CompanyDto> findLastCompanyWithEM() {
        // create the outer query
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        var cq = cb.createQuery(Company.class);
        var root = cq.from(Company.class);

        //Podzapytanie
        var sub = cq.subquery(Long.class);
        var subRoot = sub.from(Company.class);
        sub.select(cb.max(subRoot.get("companyId")));

        cq.where(cb.equal(root.get("companyId"), sub));

        var query = entityManager.createQuery(cq);
        return Optional.of(query.getSingleResult())
                .map(Company::toDto);
    }

    public List<CompanyDto> findCompaniesWithManyEmployees() {
        return repository.findCompaniesWithManyEmployees()
                .stream()
                .map(Company::toDto)
                .collect(Collectors.toList());
    }
}
