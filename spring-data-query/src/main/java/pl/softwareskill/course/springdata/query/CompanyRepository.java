package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    //Bez FETCH będzie problem N+1
    @Query("select c from Company c LEFT JOIN FETCH c.employees e where e.lastName= :lastName and e.firstName = :firstName")
    List<Company> findCompaniesForEmployee(String lastName, String firstName);

    @Query("SELECT c.name as name, AVG(e.age) as avgAge\n" +
            "FROM Company c \n" +
            "JOIN c.employees e \n" +
            "GROUP BY c.name \n" +
            "HAVING AVG(e.age) > (SELECT AVG(e2.age) FROM Employee e2)")
    List<CompanyAvgAge> findCompanyAvgAge();

    @Query("SELECT c FROM Company c \n" +
            "WHERE (SELECT COUNT(e) FROM c.employees e) >= 2")
    List<Company> findCompaniesWithManyEmployees();

    @Query("SELECT c FROM Company c \n" +
            "WHERE c.id = (SELECT max(c2.id) FROM Company c2)")
    Optional<Company> findLastCompany();
}
