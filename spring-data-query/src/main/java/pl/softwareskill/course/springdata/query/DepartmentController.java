package pl.softwareskill.course.springdata.query;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.query.dto.AddressDto;
import pl.softwareskill.course.springdata.query.dto.DepartmentDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL, JPQL i CriteriaAPI
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @PutMapping("/{id}/changeAddress")
    void updateDepartmentsAddress(@RequestBody AddressDto addressDto, @PathVariable("id") Long departmentId) {
        facade.updateDepartmentsAddress(departmentId, addressDto);
    }

    @DeleteMapping("/byCity")
    void deleteByCity(@RequestParam("city") String city) {
        facade.deleteByCity(city);
    }

    @GetMapping("/byAddressAndSharedManager")
    ResponseEntity<List<DepartmentDto>> byAddressAndSharedManager(@RequestParam("city") String city, @RequestParam("postalCode") String postalCode) {
        return ResponseEntity.ok(facade.getByAddressHavingSharedManager(city, postalCode));
    }

    @DeleteMapping("/{departmentId}")
    ResponseEntity<Object> deleteUsingCriteria(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithCriteria(new DepartmentId(departmentId));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/unlinkManager/{managerId}")
    ResponseEntity<Object> unlinkManagerUsingCriteria(@PathVariable("managerId") Long managerId) {
        facade.unlinkManagerWithCriteria(new ManagerId(managerId));
        return ResponseEntity.ok().build();
    }
}
