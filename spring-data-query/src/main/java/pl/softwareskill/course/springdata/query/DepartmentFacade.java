package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.transaction.Transactional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.springdata.query.dto.AddressDto;
import pl.softwareskill.course.springdata.query.dto.DepartmentDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;

    @Transactional
    public void updateDepartmentsAddress(Long departmentId, AddressDto newAddress) {
        //To wywołanie się nie powiedzie, zobacz jakie zapytanie się wygeneruje i jaki błąd wystąpi
        //departmentRepository.updateDepartmentsManagerLastName(List.of("AAAA"), "KKK");
        departmentRepository.updateDepartmentsAddressCity(departmentId, newAddress.getCity());
    }

    @Transactional
    public void deleteByCity(String city) {

        departmentRepository.deleteByCity(city);
    }

    @Transactional
    public List<DepartmentDto> getByAddressHavingSharedManager(String city, String postalCode) {

        return departmentRepository.getByAddressHavingSharedManager(city, postalCode)
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

    @org.springframework.transaction.annotation.Transactional
    public void deleteWithCriteria(DepartmentId departmentId) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();

        CriteriaDelete<Department> delete = cb.createCriteriaDelete(Department.class);

        var root = delete.from(Department.class);

        delete.where(cb.equal(root.get("departmentId"), departmentId.getValue()));
        Query deleteQuery = this.entityManager.createQuery(delete);
        var deletedCount = deleteQuery.executeUpdate();

        log.info("Usunieto {} departamentow", deletedCount);
    }

    @org.springframework.transaction.annotation.Transactional
    public void unlinkManagerWithCriteria(ManagerId managerId) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();

        CriteriaUpdate<Department> update = cb.createCriteriaUpdate(Department.class);

        var root = update.from(Department.class);

        update.set("manager", null);
        update.where(cb.equal(root.get("manager").get("managerId"), managerId.getValue()));

        Query updateQuery = this.entityManager.createQuery(update);
        var updatedCount = updateQuery.executeUpdate();

        log.info("Odpieto managera {} z {} departamentow", managerId.getValue(), updatedCount);
    }
}
