package pl.softwareskill.course.springdata.query;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    //Nieprawidłowe zapytanie - zostanie rzucony wyjątek. Drugi argument przeciw takiemu zastosowaniu
    // to to, że aktualizacja managera powinna być w ManagerRepository (SRP Principle)
    @Query("update Department d set d.manager.lastName = :lastName where d.name in (:names)")
    @Modifying
    void updateDepartmentsManagerLastName(List<String> names, String lastName);

    @Query("update Department d set d.address.city =:city  where d.id=:id")
    //Alias id jest dostępny i zadziała -  możesz użyć zarówno departmentId jak i id
//    @Query("update Department d set d.address.city =:city  where d.departmentId=:id")
    @Modifying
    void updateDepartmentsAddressCity(Long id, String city);

    @Query("delete from Department where address.city =:city")
    @Modifying
    void deleteByCity(String city);

    @Query("from Department d LEFT JOIN FETCH d.manager where (d.address.city =:city and d.address.postalCode = :postalCode) " +
            " and exists (select 1 from Department d1 where d1.manager = d.manager and d.id != d1.id)")
    @Modifying
    List<Department> getByAddressHavingSharedManager(String city, String postalCode);

}
