package pl.softwareskill.course.springdata.query;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class JasyptCryptorConfig {

    @Bean(name = "jasyptStringEncryptor")
    public StringEncryptor getPasswordEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();

        config.setPassword("JavaDeveloper_v1"); // klucz prywatny do szyfrowania

        config.setAlgorithm("PBEWithMD5AndDES");
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setStringOutputType("base64");

        encryptor.setConfig(config);

        return encryptor;
    }

    public static void main(String[] args) {
        System.out.println(new JasyptCryptorConfig().getPasswordEncryptor().encrypt("softwareskill"));
        System.out.println(new JasyptCryptorConfig().getPasswordEncryptor().decrypt("BtVfp/Vpo5AQB79gvrckn5xQv2VQ71A+"));
        System.out.println(new JasyptCryptorConfig().getPasswordEncryptor().decrypt("L7q/bJ/SXfObYP5J+Ati/XuHDDn9DE8K"));
    }
}
