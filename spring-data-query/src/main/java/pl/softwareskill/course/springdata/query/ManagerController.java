package pl.softwareskill.course.springdata.query;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL, JPQL i CriteriaAPI
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/managers")
@SuppressWarnings("unused")
public class ManagerController {

    ManagerFacade facade;

    @PutMapping("/{id}")
    void updateDepartmentsManagerLastName(@PathVariable("id") Long departmentId, @RequestParam("lastName") String lastName) {
        facade.updateManagersLastName(departmentId, lastName);
    }
}
