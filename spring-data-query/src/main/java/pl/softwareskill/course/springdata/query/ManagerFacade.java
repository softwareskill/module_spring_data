package pl.softwareskill.course.springdata.query;

import javax.transaction.Transactional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class ManagerFacade {

    ManagerRepository managerRepository;

    //Zamiast prostego typu Long opakuj managerId klasą
    @Transactional
    public void updateManagersLastName(Long managerId, String lastName) {
        managerRepository.updateManagerLastName(managerId, lastName);
    }
}
