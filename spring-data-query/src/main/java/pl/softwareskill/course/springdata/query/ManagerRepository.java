package pl.softwareskill.course.springdata.query;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Long> {

    //Przykład, mozna by było to zrealizowac poprzez zwykły update odczytanej encji
    @Query("update Manager set lastName = :lastName where id=:id")
    @Modifying
    void updateManagerLastName(Long id, String lastName);
}
