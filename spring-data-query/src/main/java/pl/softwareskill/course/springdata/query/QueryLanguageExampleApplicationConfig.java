package pl.softwareskill.course.springdata.query;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class QueryLanguageExampleApplicationConfig {

    @Bean
    CardFacade cardsFacade(EntityManager entityManager) {
        return new CardFacade(entityManager);
    }

    @Bean
    UserFacade userFacade(UserRepository userRepository, EntityManager entityManager) {
        return new UserFacade(userRepository, entityManager);
    }

    @Bean
    VehicleFacade vehicleFacade(VehicleRepository vehicleRepository) {
        return new VehicleFacade(vehicleRepository);
    }

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new DepartmentFacade(departmentRepository, entityManager);
    }

    @Bean
    ManagerFacade managerFacade(ManagerRepository managerRepository) {
        return new ManagerFacade(managerRepository);
    }

    @Bean
    CompanyFacade companyFacade(CompanyRepository companyRepository, EntityManager entityManager) {
        return new CompanyFacade(companyRepository, entityManager);
    }
}
