package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.springdata.query.dto.UserDto;

@Entity
@Table(name = "USERS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class User {
    @Id
    @Column(name = "USER_ID")
    String userId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
    List<Card> cards;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "EMAIL_ADDRESS")
    @CollectionTable(name = "USER_EMAILS", joinColumns = @JoinColumn(name = "USER_ID"))
    Set<String> emails;

    public UserDto toDto() {
        return UserDto.builder()
                .firstName(getFirstName())
                .lastName(getLastName())
                .build();
    }
}
