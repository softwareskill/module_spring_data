package pl.softwareskill.course.springdata.query;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.query.dto.UserDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL, JPQL i CriteriaAPI
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/users/queries")
@SuppressWarnings("unused")
public class UserController {

    UserFacade facade;

    @GetMapping("/jpql/collections")
    ResponseEntity<List<UserDto>> findAllUsersForEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok(facade.findUsersByEmail(email));
    }

    @GetMapping("/jpql/collections/havingEmail")
    ResponseEntity<List<UserDto>> findAllUsersHavingEmail() {
        return ResponseEntity.ok(facade.findUsersHavingEmail());
    }

    @GetMapping("/criteria/countUsersHavingCardForCountry")
    ResponseEntity<Long> cardsCountForCountry(@RequestParam("cardCountry") CardCountry cardCountry) {
        return ResponseEntity.ok(facade.countUsersHavingCardForCountry(cardCountry));
    }

    @GetMapping("/criteria/cardsCountByUser")
    ResponseEntity<List<Object[]>> cardsCountByUser() {
        return ResponseEntity.ok(facade.cardsCountByUser());
    }
}
