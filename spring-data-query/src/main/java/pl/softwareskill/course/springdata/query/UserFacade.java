package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.springdata.query.dto.UserDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class UserFacade {

    UserRepository userRepository;
    EntityManager entityManager;

    public List<UserDto> findUsersByEmail(String email) {
        return userRepository.findByEmail(email)
                .stream()
                .map(User::toDto)
                .collect(Collectors.toList());
    }

    public List<UserDto> findUsersHavingEmail() {
        return userRepository.findHavingEmail()
                .stream()
                .map(User::toDto)
                .collect(Collectors.toList());
    }

    public Long countUsersHavingCardForCountry(CardCountry country) {

        var builer = entityManager.getCriteriaBuilder();

        var criteriaQuery = builer.createQuery(Long.class);

        var root = criteriaQuery.from(User.class);
        var cardJoin = root.join("cards");

        criteriaQuery.where(builer.equal(cardJoin.get("cardCountry"), country));

        criteriaQuery.distinct(true);

        criteriaQuery.select(builer.count(root));

        var query = entityManager.createQuery(criteriaQuery);

        return query.getSingleResult();
    }

    public List<Object[]> cardsCountByUser() {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<User> userRoot = cq.from(User.class);
        var join = userRoot.join("cards");

        cq.multiselect(userRoot.get("userId"), cb.count(join))
                .groupBy(userRoot.get("userId"))
                .having(cb.ge(cb.count(join), 2));

        TypedQuery<Object[]> query = entityManager.createQuery(cq);
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }
}
