package pl.softwareskill.course.springdata.query;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, String> {

    @Query("select u from User u where :email member of u.emails")
    List<User> findByEmail(String email);

    @Query("select u from User u where u.emails is not empty")
    List<User> findHavingEmail();
}
