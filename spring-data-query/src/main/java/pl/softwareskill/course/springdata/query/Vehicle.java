package pl.softwareskill.course.springdata.query;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
@Table(name = "VEHICLES")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class Vehicle {
    @Id
    Long id;

    @Column(name = "WHEELS_COUNT")
    int wheelsCount;
}
