package pl.softwareskill.course.springdata.query;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.query.dto.CarDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej HQL, JPQL i CriteriaAPI
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/vehicles/cars")
@SuppressWarnings("unused")
public class VehicleController {

    VehicleFacade facade;

    @GetMapping
    ResponseEntity<List<CarDto>> findAllCars() {
        return ResponseEntity.ok(facade.findAllCars());
    }
}
