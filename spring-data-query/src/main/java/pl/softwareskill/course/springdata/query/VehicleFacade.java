package pl.softwareskill.course.springdata.query;

import java.util.List;
import java.util.stream.Collectors;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.springdata.query.dto.CarDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class VehicleFacade {

    VehicleRepository repository;

    public List<CarDto> findAllCars() {
        return repository.getAllCars()
                .stream()
                .map(Car::toDto)
                .collect(Collectors.toList());
    }
}
