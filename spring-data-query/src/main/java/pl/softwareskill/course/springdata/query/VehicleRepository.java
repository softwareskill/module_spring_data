package pl.softwareskill.course.springdata.query;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

    @Query("select v from Vehicle v where type(v) = Car")
    List<Car> getAllCars();
}
