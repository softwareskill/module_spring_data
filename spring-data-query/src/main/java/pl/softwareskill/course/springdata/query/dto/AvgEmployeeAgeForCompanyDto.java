package pl.softwareskill.course.springdata.query.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class AvgEmployeeAgeForCompanyDto {

    String name;
    Long avgAge;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public AvgEmployeeAgeForCompanyDto(String name, Long avgAge) {
        this.name = name;
        this.avgAge = avgAge;
    }
}
