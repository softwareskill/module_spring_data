package pl.softwareskill.course.springdata.query.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.query.CardCountry;

@Getter
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class CardDto {
    String cardUuid;

    Boolean enabled;

    CardCountry cardCountry;

    UserDto owner;

    Instant expiresAt;

    BigDecimal balance;
}
