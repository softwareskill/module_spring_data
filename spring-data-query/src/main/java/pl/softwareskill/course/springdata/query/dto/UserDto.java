package pl.softwareskill.course.springdata.query.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static java.util.Objects.isNull;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.query.User;

@Getter
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class UserDto {
    String firstName;

    String lastName;

    public static UserDto toDto(User owner) {
        if (isNull(owner)) {
            return null;
        }
        return UserDto.builder()
                .lastName(owner.getLastName())
                .firstName(owner.getFirstName())
                .build();
    }
}
