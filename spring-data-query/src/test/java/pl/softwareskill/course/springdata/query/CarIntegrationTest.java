package pl.softwareskill.course.springdata.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CarIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindCars() throws Exception {

        mvc.perform(get("/vehicles/cars")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
