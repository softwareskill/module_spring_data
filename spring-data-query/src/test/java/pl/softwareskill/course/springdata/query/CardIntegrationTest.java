package pl.softwareskill.course.springdata.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CardIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindCardsCountExpiringForYearUsingHQL() throws Exception {
        final Integer year = 2020;

        mvc.perform(get("/cards/queries/hql?year={year}", year)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindCardsCountExpiringForYearUsingJPQL() throws Exception {
        final Integer year = 2020;

        mvc.perform(get("/cards/queries/jpql?year={year}", year)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindAllVIPCardsUsingCriteria() throws Exception {
        mvc.perform(get("/cards/queries/criteria/allVIPCards")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindAllCards() throws Exception {
        mvc.perform(get("/cards/queries/criteria/findAll")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindAllCardsExpiringInAYearWithBalance() throws Exception {
        mvc.perform(get("/cards/queries/criteria/expiringInAYearWithBalance?minBalance=12.33&maxBalance=456.89")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
