package pl.softwareskill.course.springdata.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CompanyIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindCompaniesForEmployee() throws Exception {
        final String lastName = "Kadziolka";
        final String firstName = "Krzysztof";

        mvc.perform(get("/companies/companiesForEmployee?lastName={lastNamer}&firstName={firstName}", lastName, firstName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindAvgEmployeeAgeByCompany() throws Exception {
        mvc.perform(get("/companies/avgEmployeeAge")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindAvgEmployeeAgeByCompanyWithEM() throws Exception {
        mvc.perform(get("/companies/avgEmployeeAgeWithEM")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindLastCompany() throws Exception {
        mvc.perform(get("/companies/last")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindLastCompanyWithEM() throws Exception {
        mvc.perform(get("/companies/lastWithEM")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindCompaniesWithManyEmployees() throws Exception {
        mvc.perform(get("/companies/withManyEmployees")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
