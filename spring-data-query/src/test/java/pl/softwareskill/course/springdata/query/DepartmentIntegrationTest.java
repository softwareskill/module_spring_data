package pl.softwareskill.course.springdata.query;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import pl.softwareskill.course.springdata.query.dto.AddressDto;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DepartmentIntegrationTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void shouldUpdateDepartmentAddress() throws Exception {
        final Long existingDepartmentId = 1L;
        var address = AddressDto.builder()
                .city("Katowice")
                .postalCode("56-900")
                .street("Alaska")
                .streetNumber("1A")
                .country("PL")
                .build();
        mvc.perform(put("/departments/{id}/changeAddress", existingDepartmentId)
                .content(objectMapper.writeValueAsString(address))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteDepartmentsForCity() throws Exception {
        final String existingCity = "Katowice";
        mvc.perform(delete("/departments/byCity?city={city}", existingCity)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindDepartmentsByAddressAndSharedManager() throws Exception {
        final String existingCity = "Katowice";
        final String existingPostalCode = "40-900";
        mvc.perform(get("/departments/byAddressAndSharedManager?city={city}&postalCode={postalCode}", existingCity, existingPostalCode)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteDepartmentWithCriteria() throws Exception {
        Long existingDepartmentId = 1L;

        mvc.perform(delete("/departments/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUnlinkManagerWithCriteria() throws Exception {
        Long existingManagerId = 1L;

        mvc.perform(put("/departments/unlinkManager/{managerId}", existingManagerId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
