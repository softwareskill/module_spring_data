package pl.softwareskill.course.springdata.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ManagerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldUpdateManagerLastName() throws Exception {
        final Long existingManagerId = 1L;
        final String lastName = "LastName" + System.currentTimeMillis();

        mvc.perform(put("/managers/{id}?lastName={lastName}", existingManagerId, lastName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
