package pl.softwareskill.course.springdata.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = QueryLanguageExampleApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void shouldFindUserByEmailUsingJPQL() throws Exception {
        final String existingEmail = "krzysztof.kadziolka@gmail.com";

        mvc.perform(get("/users/queries/jpql/collections?email={email}", existingEmail)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindUserHavingEmail() throws Exception {
        mvc.perform(get("/users/queries/jpql/collections/havingEmail")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldCountUsersHavingCardForCountry() throws Exception {
        String existingCountry = "PL";
        mvc.perform(get("/users/queries/criteria/countUsersHavingCardForCountry?cardCountry={country}", existingCountry)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnCardsCountByUser() throws Exception {
        mvc.perform(get("/users/queries/criteria/cardsCountByUser")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
