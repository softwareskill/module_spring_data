CREATE TABLE TABLE_AUTOGENERATE (
   SEQUENCE_NAME VARCHAR(100) NOT NULL PRIMARY KEY,
   NEXT_VAL DECIMAL(22) NOT NULL
);

CREATE TABLE MANAGERS (
    MANAGER_ID DECIMAL(22) NOT NULL PRIMARY KEY,
    FIRST_NAME VARCHAR(100) NOT NULL,
    LAST_NAME VARCHAR(100) NOT NULL
);

CREATE SEQUENCE SEQ_MANAGERS INCREMENT BY 50 MINVALUE 1000;

CREATE TABLE DEPARTMENTS (
    DEPARTMENT_ID DECIMAL(22) NOT NULL PRIMARY KEY,
    MANAGER_ID DECIMAL(22),
    NAME VARCHAR(100) NOT NULL,
     --Embedded address
    STREET VARCHAR(100) NOT NULL,
    STREET_NUMBER VARCHAR(10) NOT NULL,
    CITY VARCHAR(100) NOT NULL,
    COUNTRY VARCHAR(2) NOT NULL,
    POSTAL_CODE VARCHAR(6) NOT NULL
);

ALTER TABLE DEPARTMENTS ADD FOREIGN KEY (MANAGER_ID) REFERENCES MANAGERS(MANAGER_ID);

CREATE SEQUENCE SEQ_DEPARTMENTS INCREMENT BY 50 MINVALUE 1000;



CREATE TABLE USERS (
    USER_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    FIRST_NAME VARCHAR(40) NOT NULL,
    LAST_NAME VARCHAR(100) NOT NULL,
    PASSWORD VARCHAR(255)
);

CREATE TABLE CARDS (
    CARD_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    CARD_UUID VARCHAR(40) NOT NULL,
    CARD_OWNER_ID VARCHAR(100) NOT NULL,
    ENABLED VARCHAR(1) NOT NULL,
    COUNTRY VARCHAR(2) NOT NULL,
    BALANCE DECIMAL(10,2) NOT NULL DEFAULT 0,
    EXPIRES_AT DATE,
    CREATED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UPDATED_AT TIMESTAMP,
    PNG_IMAGE BYTEA
);

ALTER TABLE CARDS ADD FOREIGN KEY (CARD_OWNER_ID) REFERENCES USERS(USER_ID);

CREATE SEQUENCE SEQ_CARDS_50 INCREMENT BY 50 MINVALUE 1000;
CREATE SEQUENCE SEQ_CARDS_20 INCREMENT BY 20 MINVALUE 1000;

CREATE TABLE USER_EMAILS (
    EMAIL_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    EMAIL_ADDRESS VARCHAR(100) NOT NULL,
    USER_ID VARCHAR(100) NOT NULL
);

ALTER TABLE USER_EMAILS  ADD FOREIGN KEY (USER_ID)  REFERENCES USERS(USER_ID);

CREATE TABLE VEHICLES (
    ID DECIMAL(22,0) NOT NULL PRIMARY KEY,
    TYPE VARCHAR(10) NOT NULL,
    WHEELS_COUNT DECIMAL(1,0) NOT NULL,
    GEAR_COUNT DECIMAL(1,0),
    CAR_BRAND VARCHAR(50),
    FRONT_COUNT DECIMAL(1,0),
    REAR_COUNT DECIMAL(1,0)
);


CREATE TABLE COMPANIES (
    COMPANY_ID DECIMAL(22) NOT NULL PRIMARY KEY,
    NAME VARCHAR(100) NOT NULL
);

CREATE SEQUENCE SEQ_COMPANIES INCREMENT BY 50 MINVALUE 1000;

CREATE TABLE EMPLOYEES (
    EMPLOYEE_ID DECIMAL(22) NOT NULL PRIMARY KEY,
    FIRST_NAME VARCHAR(100) NOT NULL,
    LAST_NAME VARCHAR(100) NOT NULL,
    AGE DECIMAL(3) NOT NULL
);

CREATE SEQUENCE SEQ_EMPLOYEES INCREMENT BY 50 MINVALUE 1000;

CREATE TABLE EMPLOYEE_COMPANY (
    COMPANY_ID DECIMAL(22) NOT NULL,
    EMPLOYEE_ID DECIMAL(22) NOT NULL
);

ALTER TABLE EMPLOYEE_COMPANY ADD PRIMARY KEY (COMPANY_ID,EMPLOYEE_ID);
ALTER TABLE EMPLOYEE_COMPANY ADD FOREIGN KEY (COMPANY_ID) REFERENCES COMPANIES(COMPANY_ID);
ALTER TABLE EMPLOYEE_COMPANY ADD FOREIGN KEY (EMPLOYEE_ID) REFERENCES EMPLOYEES(EMPLOYEE_ID);