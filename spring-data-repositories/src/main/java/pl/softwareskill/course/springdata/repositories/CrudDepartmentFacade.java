package pl.softwareskill.course.springdata.repositories;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.repositories.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentCreationStatus;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Ponieważ jest modyfikacja musi być Transactional oraz metody publiczne
public class CrudDepartmentFacade {

    CrudDepartmentRepository repository;

    @Transactional
    public DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto) {

        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        var validator = new DepartmentValidator(repository);
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        var department = Department.fromDto(createDepartmentDto);

        //Użycie metody z CrudRepository
        repository.save(department);
        return DepartmentCreationStatus.CREATED;
    }
}
