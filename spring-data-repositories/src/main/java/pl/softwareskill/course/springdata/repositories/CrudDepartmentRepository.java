package pl.softwareskill.course.springdata.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrudDepartmentRepository extends CrudRepository<Department, Long> {

    boolean existsByNameIgnoreCase(String departmentName);
}
