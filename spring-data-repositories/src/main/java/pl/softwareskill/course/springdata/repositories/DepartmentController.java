package pl.softwareskill.course.springdata.repositories;

import java.util.List;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z wyszukowaniem i usuwaniem departamentów
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class DepartmentController {

    DepartmentFacade facade;

    @DeleteMapping("/byQuery/{departmentId}")
    ResponseEntity<Object> deleteUsingQuery(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithQuery(departmentId);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    ResponseEntity<List<DepartmentDto>> findByName(@RequestParam("name") String name) {
        var departments = facade.findDepartments(name);
        return ResponseEntity.ok().body(departments);
    }
}
