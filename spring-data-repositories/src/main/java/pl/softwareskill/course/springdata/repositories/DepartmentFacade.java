package pl.softwareskill.course.springdata.repositories;

import java.util.List;
import java.util.stream.Collectors;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
public class DepartmentFacade {

    DepartmentRepository repository;

    @Transactional
    public void deleteWithQuery(Long departmentId) {
        //Anotacja Query w Repository
        repository.deleteByQueryAnnotation(departmentId);
    }

    //Operacja na strumieniu z repository wymaga Transactional
    @Transactional
    public List<DepartmentDto> findDepartments(String departmentName) {
        return repository.getByNameLikeOrderByNameAscAddress_CityDesc(departmentName)
                .map(Department::toDto)
                .collect(Collectors.toList());
    }
}
