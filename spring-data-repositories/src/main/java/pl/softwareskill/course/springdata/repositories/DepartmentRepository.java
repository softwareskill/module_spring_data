package pl.softwareskill.course.springdata.repositories;

import java.util.List;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface DepartmentRepository extends Repository<Department, Long> {

    void save(Department department);

    void save(List<Department> department);

    //saveAll nie zadziała z List
    void saveAll(Iterable<Department> department);

    void delete(Department department);

    long countDistinctByName(String name);

    long countDistinctAddress_cityByName(String name);

    boolean existsByAddress_Street(String street);

    Stream<Department> getByNameEqualsOrderByNameAscAddress_CityDesc(String name);

    Stream<Department> getByNameLikeOrderByNameAscAddress_CityDesc(String name);

    List<Department> findAll();

    List<Department> name(String name);

    List<Department> searchByName(String name);

    List<Department> findAllByName(String name);

    List<Department> findByName(String name);

    Stream<Department> getByName(String name);

    Stream<Department> getByNameIs(String name);

    Stream<Department> getByNameEquals(String name);

    void deleteAll();

    //Z listą nie zadziała
    void deleteAll(Iterable<Department> departments);

    void deleteAllByName(String name);

    void deleteAllByNameIn(List<String> names);

    //Bez anotacji @Modyfying zostanie rzucony wyjątek
    @Modifying
    //alias id jest dostepny dla JPA - reprezentuje klucz główny encji
    @Query("delete from Department d where d.id = :departmentId")
    void deleteByQueryAnnotation(Long departmentId);
}
