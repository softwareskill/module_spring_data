package pl.softwareskill.course.springdata.repositories;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentDto;
import pl.softwareskill.course.springdata.repositories.dto.PageableSearchResultDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z wyszukiwaniem departamentów  oraz stronicowaniem
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments")
@SuppressWarnings("unused")
public class JpaDepartmentController {

    JpaDepartmentFacade facade;

    @GetMapping("/page")
    ResponseEntity<PageableSearchResultDto<DepartmentDto>> findPage(@RequestParam("pageNum") int pageNum) {
        var pageResult = facade.findPage(pageNum);
        return ResponseEntity.ok().body(pageResult);
    }
}
