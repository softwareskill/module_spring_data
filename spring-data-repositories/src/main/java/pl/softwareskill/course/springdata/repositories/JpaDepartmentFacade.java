package pl.softwareskill.course.springdata.repositories;

import java.util.List;
import java.util.stream.Collectors;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentDto;
import pl.softwareskill.course.springdata.repositories.dto.PageableSearchResultDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
//Ponieważ jest modyfikacja musi być Transactional oraz metody publiczne
public class JpaDepartmentFacade {

    JpaDepartmentRepository repository;

    public PageableSearchResultDto<DepartmentDto> findPage(int pageNumber) {
        Pageable pageWithTwoElements = PageRequest.of(pageNumber, 2, Sort.by(Department_.NAME));

        var departmentPage = repository.findAll(pageWithTwoElements);

        List<DepartmentDto> departments = getDepartmentDtos(departmentPage);

        return toPageableResult(departmentPage, departments);
    }

    private PageableSearchResultDto<DepartmentDto> toPageableResult(Page<Department> departmentPage, List<DepartmentDto> departments) {
        return PageableSearchResultDto.<DepartmentDto>builder()
                .items(departments)
                .numberOfElements(departmentPage.getNumberOfElements())
                .pageNumber(departmentPage.getNumber())
                .totalPages(departmentPage.getTotalPages())
                .totalElements(departmentPage.getTotalElements())
                .build();
    }

    private List<DepartmentDto> getDepartmentDtos(Page<Department> departmentPage) {
        return departmentPage
                .stream()
                .map(Department::toDto)
                .collect(Collectors.toList());
    }

}
