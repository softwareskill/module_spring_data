package pl.softwareskill.course.springdata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaDepartmentRepository extends JpaRepository<Department, Long> {
}
