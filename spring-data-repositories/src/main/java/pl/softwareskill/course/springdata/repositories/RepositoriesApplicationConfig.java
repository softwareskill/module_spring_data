package pl.softwareskill.course.springdata.repositories;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class RepositoriesApplicationConfig {

    @Bean
    DepartmentFacade departmentFacade(DepartmentRepository departmentRepository) {
        return new DepartmentFacade(departmentRepository);
    }

    @Bean
    CrudDepartmentFacade crudDepartmentFacade(CrudDepartmentRepository crudDepartmentRepository) {
        return new CrudDepartmentFacade(crudDepartmentRepository);
    }

    @Bean
    JpaDepartmentFacade jpaDepartmentFacade(JpaDepartmentRepository jpaDepartmentRepository) {
        return new JpaDepartmentFacade(jpaDepartmentRepository);
    }
}
