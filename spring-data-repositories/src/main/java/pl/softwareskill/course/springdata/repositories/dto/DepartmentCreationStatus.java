package pl.softwareskill.course.springdata.repositories.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    INVALID_INPUT
}
