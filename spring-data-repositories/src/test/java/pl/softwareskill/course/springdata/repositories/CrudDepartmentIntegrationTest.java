package pl.softwareskill.course.springdata.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import pl.softwareskill.course.springdata.repositories.dto.AddressDto;
import pl.softwareskill.course.springdata.repositories.dto.CreateDepartmentDto;

public class CrudDepartmentIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    @Test
    void shouldNotCreateNonUniqueDepartment() throws Exception {
        String existingDepartmentName = "Hibernate" + System.nanoTime();

        existingDepartment(existingDepartmentName);

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    private void existingDepartment(String existingDepartmentName) throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .build();
        mvc.perform(post("/departments")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
