package pl.softwareskill.course.springdata.repositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import pl.softwareskill.course.springdata.repositories.dto.AddressDto;
import pl.softwareskill.course.springdata.repositories.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.repositories.dto.DepartmentCreationStatus;

public class DepartmentCreateTest {

    private CrudDepartmentRepository repository;

    private CrudDepartmentFacade facade;

    @BeforeEach
    void setup() {
        repository = Mockito.mock(CrudDepartmentRepository.class);
        facade = new RepositoriesApplicationConfig().crudDepartmentFacade(repository);
    }

    @Test
    void shouldCreateDepartment() {
        var departmentToCreate = existingDepartment();

        var result = facade.createDepartment(departmentToCreate);
        Assertions.assertEquals(result, DepartmentCreationStatus.CREATED);
    }

    private CreateDepartmentDto existingDepartment() {
        CreateDepartmentDto sampleDepartment = sampleCreateDepartmentDto(sampleUniqueDepartmentName());
        facade.createDepartment(sampleDepartment);
        return sampleDepartment;
    }

    private static CreateDepartmentDto sampleCreateDepartmentDto(String name) {
        return CreateDepartmentDto.builder()
                .name(name)
                .address(sampleAddress())
                .build();
    }

    @Test
    void shouldNotCreateNonUniqueDepartment() {
        String existingDepartmentName = sampleUniqueDepartmentName();

        Mockito.when(repository.existsByNameIgnoreCase(eq(existingDepartmentName)))
                .thenReturn(true);

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        var result = facade.createDepartment(createDepartmentDto);
        Assertions.assertEquals(DepartmentCreationStatus.ALREADY_EXISTS, result);
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() {
        CreateDepartmentDto createDepartmentDto = invalidCreateDepartmentRequest();

        var result = facade.createDepartment(createDepartmentDto);
        Assertions.assertEquals(DepartmentCreationStatus.INVALID_INPUT, result);
    }

    private static CreateDepartmentDto invalidCreateDepartmentRequest() {
        return CreateDepartmentDto.builder().build();
    }

    private static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }

    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
