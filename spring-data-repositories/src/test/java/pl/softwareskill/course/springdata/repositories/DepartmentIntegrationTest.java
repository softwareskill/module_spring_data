package pl.softwareskill.course.springdata.repositories;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DepartmentIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldDeleteDepartment() throws Exception {
        Long existingDepartmentId = 1L;

        mvc.perform(delete("/departments/byQuery/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindDepartments() throws Exception {
        existingDepartmentId();
        var departmentNamePrefix = "%T%";

        mvc.perform(get("/departments?name={departmentNamePrefix}", departmentNamePrefix)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].address.city", anyStringMatcher()));
    }

    private Long existingDepartmentId() {
        return sampleDepartmentData.createSampleDepartment();
    }

    private static Matcher<String> anyStringMatcher() {
        return CoreMatchers.any(String.class);
    }

}
