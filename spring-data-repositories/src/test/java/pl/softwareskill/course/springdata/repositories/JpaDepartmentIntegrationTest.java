package pl.softwareskill.course.springdata.repositories;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class JpaDepartmentIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldFindDepartmentsPage() throws Exception {
        existingDepartmentId();

        int existingPageResultNumber = 0;

        mvc.perform(get("/departments/page?pageNum=" + existingPageResultNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalElements", anyNumberMatcher()))
                .andExpect(jsonPath("$.totalPages", anyNumberMatcher()))
                .andExpect(jsonPath("$.numberOfElements", CoreMatchers.equalTo(2)))
                .andExpect(jsonPath("$.pageNumber", CoreMatchers.equalTo(existingPageResultNumber)));
    }

    private Long existingDepartmentId() {
        return sampleDepartmentData.createSampleDepartment();
    }

    private static Matcher<Number> anyNumberMatcher() {
        return CoreMatchers.any(Number.class);
    }
}
