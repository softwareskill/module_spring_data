package pl.softwareskill.course.springdata.repositories;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.repositories.dto.AddressDto;

@Component
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Transactional
class SampleDepartmentData {

    CrudDepartmentRepository repository;

    public Long createSampleDepartment() {
        return createAndSaveDepartment("Test" + System.nanoTime());
    }

    private Long createAndSaveDepartment(String departmentName) {
        var department = new Department();
        department.setName(departmentName);
        department.setAddress(Address.fromDto(sampleAddress()));
        department = repository.save(department);
        return department.getDepartmentId();
    }


    private static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }
}
