package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class AnotherTransactionalService {

    DepartmentRepository repository;

    @Transactional
    public Optional<String> searchDepartmentName(Long departmentId) {
        return repository.findById(departmentId)
                .map(Department::getName);
    }

    @Transactional
    void deleteDepartment(Long departmentId) {
        repository.deleteById(departmentId);
    }
}
