package pl.softwareskill.course.springdata.transactions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
@SuppressWarnings("unused")
class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CreateDepartmentException.class)
    @SuppressWarnings("unused")
    public ResponseEntity<Object> handleCreateDepartmentException(CreateDepartmentException ex, WebRequest request) {
        log.error("Blad podczas tworzenia departamanetu ", ex);
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(Exception.class)
    @SuppressWarnings("unused")
    public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request) {
        log.error("Blad podczas operacji", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
