package pl.softwareskill.course.springdata.transactions;

class CreateDepartmentException extends RuntimeException {

    public CreateDepartmentException(String message) {
        super(message);
    }
}
