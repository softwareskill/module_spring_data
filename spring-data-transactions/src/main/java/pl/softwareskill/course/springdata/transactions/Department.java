package pl.softwareskill.course.springdata.transactions;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
class Department {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANAGER_ID")
    Manager manager;

    static Department fromDto(CreateDepartmentDto createDepartmentDto) {
        var department = new Department();
        department.setName(createDepartmentDto.getName());
        department.setAddress(Address.fromDto(createDepartmentDto.getAddress()));
        return department;
    }

    DepartmentDto toDto() {
        //Ta operacja sie nie powiedzie bez transakcji
        manager.getFirstName();
        return DepartmentDto.builder()
                .name(name)
                .address(Address.toDto(address))
                .build();
    }
}
