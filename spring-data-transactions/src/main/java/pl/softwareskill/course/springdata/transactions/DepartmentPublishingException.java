package pl.softwareskill.course.springdata.transactions;

class DepartmentPublishingException extends RuntimeException {

    public DepartmentPublishingException(String message) {
        super(message);
    }
}
