package pl.softwareskill.course.springdata.transactions;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    boolean existsByNameIgnoreCase(String name);

    @Query("update Department set name=:name where id=:departmentId")
//    @Modifying //Bez modifying zostanie rzucony wyjątek Not supported for DML operations
    void updateDepartmentName(Long departmentId, String name);

    @Query("update Department set name=:name where id=:departmentId")
        //Bez modifying zostanie rzucony wyjątek Not supported for DML operations
    void updateWithoutModifyingDepartmentName(Long departmentId, String name);

}
