package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z obsługą wyjątków dla anotacji @Transactional
 * z wykorzystaniem @Transactional
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/exceptions")
@SuppressWarnings("unused")
public class DepartmentTransactionalExceptionHandlingController {

    private static final String EMPTY_JSON = "{}";

    DepartmentTransactionalExceptionHandlingFacade facade;

    @PostMapping
    ResponseEntity<?> createDepartment(@RequestBody CreateDepartmentDto createDepartmentDto) {
        facade.createDepartment(createDepartmentDto);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @DeleteMapping("/{departmentId}/withCatchedException")
    public ResponseEntity<?> deleteDepartmentWithCatchedException(@PathVariable("departmentId") Long departmentId) {
        facade.deleteDepartmentWithCatchedException(departmentId);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }
}
