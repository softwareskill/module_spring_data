package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class DepartmentTransactionalExceptionHandlingFacade {

    DepartmentRepository departmentRepository;
    DepartmentValidator validator;
    EventPublisher publisher;
    TransactionHelperService transactionHelperService;

    public DepartmentTransactionalExceptionHandlingFacade(DepartmentRepository departmentRepository, EventPublisher publisher,
                                                          TransactionHelperService transactionHelperService) {
        this.departmentRepository = departmentRepository;
        this.validator = new DepartmentValidator(departmentRepository);
        this.publisher = publisher;
        this.transactionHelperService = transactionHelperService;
    }

    // Konfiguracja dla wyjątków dotyczy bieżącej metody - kiedy  createDepartment rzuca wyjątek
    //
    // Jeżeli zostaną rzucone niżej w logice nie zadziała
    //
    // Ustaw dontRollbackOn tutaj i usuń w EventPublisher.publishDepartmentCreated i zobacz co się stanie
    //Pamietaj że musi być @Transactional i public w metodach niżej (ale sprawdź sobie także bez
    //@Transactional w EventPublisher.publishDepartmentCreated
    @javax.transaction.Transactional(rollbackOn = CreateDepartmentException.class, dontRollbackOn = DepartmentPublishingException.class)
    public void createDepartment(CreateDepartmentDto createDepartmentDto) {
        validateRequest(createDepartmentDto);

        var department = Department.fromDto(createDepartmentDto);
        departmentRepository.save(department);

        publishDepartmentCreated(department);
    }

    private void validateRequest(CreateDepartmentDto createDepartmentDto) {
        if (!validator.isRequestValid(createDepartmentDto)) {
            throw new CreateDepartmentException("Nieprawidlowe dane wejsciowe " + createDepartmentDto);
        }
    }

    private void publishDepartmentCreated(Department department) {
        try {
            publisher.publishDepartmentCreated(department.getName());
        } catch (Exception e) {
            log.warn("Blad publikacji", e);
        }
    }

    @Transactional
    public void deleteDepartmentWithCatchedException(Long departmentId) {
        departmentRepository.deleteById(departmentId);
        try {
            transactionHelperService.throwRuntimeException();
        } catch (Exception e) {
            log.info("Przechwycony wyjatek");
        }
        //Sprawdzamy czy SELECT i DML się powiedzie
        log.info("Przed usunieciem");
        departmentRepository.findById(2L)
                .ifPresent(department -> {
                    departmentRepository.delete(department);
                    //Wywolujemy flush
                    departmentRepository.flush();
                });
        log.info("Po usunieciu");
    }
}
