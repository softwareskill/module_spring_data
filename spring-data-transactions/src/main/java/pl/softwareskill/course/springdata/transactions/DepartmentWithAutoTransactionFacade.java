package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class DepartmentWithAutoTransactionFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;
    TransactionHelperService transactionHelperService;

    @Transactional
    //Możesz użyć anotacji z pakietu javax.transaction - Spring także ją wspiera
    //@javax.transaction.Transactional
    public DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto) {

        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        DepartmentCreationStatus processingStatus = validate(createDepartmentDto);
        if (!DepartmentCreationStatus.IN_PROGRESS.equals(processingStatus)) {
            return processingStatus;
        }
        var department = Department.fromDto(createDepartmentDto);

//        //Użycie metody z CrudRepository
        departmentRepository.save(department);
//
//        //Użycie EntityManager
        entityManager.persist(department);

        //Poniższy kod rzuci błąd - jest to shared EntityManager. Taki sposób jest nieprawidłowy
//        var transaction = entityManager.getTransaction();
//        transaction.begin();
//        entityManager.persist(department);
//        transaction.commit();

        return DepartmentCreationStatus.CREATED;
    }

    private DepartmentCreationStatus validate(CreateDepartmentDto createDepartmentDto) {
        var validator = new DepartmentValidator(departmentRepository);
        final String departmentName = createDepartmentDto.getName();
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        return DepartmentCreationStatus.IN_PROGRESS;
    }

    //ReadOnly - będzie commit ale nie będzie flush
    @Transactional(readOnly = true)
    public void deleteDepartmentAttemptWithReadOnly(Long departmentId) {
        departmentRepository.findById(departmentId)
                .ifPresent(departmentRepository::delete);
    }

    //Timeout 2s
    @Transactional(timeout = 2)
    public void findDepartmentWithLongLoad(Long departmentId) {
        //Zatrzymujemy wątek na 3s czyli więcej niż timeout dla transakcji
        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            log.error("Error", e);
        }
        departmentRepository.findById(departmentId);
    }

    @Transactional(timeout = 2, readOnly = true)
    public void deleteWithTransactionPropagation(Long departmentId) {
        //Wywolanie metody publicznej z @Transactional i innymi ustawieniami transakcji
        transactionHelperService.deleteWithTransactionOverrideAttempt(departmentId);
    }

    @Transactional
    public Optional<String> findDepartmentName(Long departmentId) {
        return transactionHelperService.searchDepartmentName(departmentId);
    }
}
