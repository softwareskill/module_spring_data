package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z tworzeniem departamentów z automatycznymi transakcjami
 * z wykorzystaniem @Transactional
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/auto")
@SuppressWarnings("unused")
public class DepartmentWithAutoTransactionsController {

    private static final String EMPTY_JSON = "{}";

    DepartmentWithAutoTransactionFacade facade;

    @PostMapping
    ResponseEntity<?> createDepartment(@RequestBody CreateDepartmentDto createDepartmentDto) {
        DepartmentCreationStatus creationStatus = facade.createDepartment(createDepartmentDto);
        switch (creationStatus) {
            case ALREADY_EXISTS:
                return ResponseEntity.status(HttpStatus.CONFLICT).body(EMPTY_JSON);
            case INVALID_INPUT:
                return ResponseEntity.badRequest().body(EMPTY_JSON);
            default:
                return ResponseEntity.ok().body(EMPTY_JSON);
        }
    }

    @DeleteMapping("/{departmentId}/readOnly")
    public ResponseEntity<?> deleteDepartmentAttemptWithReadOnly(@PathVariable("departmentId") Long departmentId) {
        facade.deleteDepartmentAttemptWithReadOnly(departmentId);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @GetMapping("/{departmentId}/timeout")
    public ResponseEntity<?> findDepartmentWithLongLoad(@PathVariable("departmentId") Long departmentId) {
        facade.findDepartmentWithLongLoad(departmentId);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @DeleteMapping("/{departmentId}/timeout")
    public ResponseEntity<?> deleteWithTransactionPropagation(@PathVariable("departmentId") Long departmentId) {
        facade.deleteWithTransactionPropagation(departmentId);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @GetMapping("/{departmentId}/name")
    public ResponseEntity<String> findDepartmentName(@PathVariable("departmentId") Long departmentId) {
        return facade.findDepartmentName(departmentId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
