package pl.softwareskill.course.springdata.transactions;

import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class DepartmentWithManualTransactionFacade {

    DepartmentRepository departmentRepository;
    EntityManager entityManager;
    PlatformTransactionManager transactionManager;

    DepartmentCreationStatus createDepartment(CreateDepartmentDto createDepartmentDto) {

        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        DepartmentCreationStatus processingStatus = validate(createDepartmentDto);
        if (!DepartmentCreationStatus.IN_PROGRESS.equals(processingStatus)) {
            return processingStatus;
        }
        var department = Department.fromDto(createDepartmentDto);

        runWithManualCommit(department);

        return DepartmentCreationStatus.CREATED;
    }

    private void runInTransactionNoResult(Department department) {
        var template = new TransactionTemplate(transactionManager);
        template.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
        template.setTimeout(30); // 30 sekund

        template.execute(new TransactionCallbackWithoutResult() {

            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    //Użycie metody z CrudRepository
                    departmentRepository.save(department);

                    //Użycie EntityManager
                    //entityManager.persist(department);
                    //Domyślnie jest commit - jeśli ustawsz setRollbackOnly transakcja zostanie wycofana
                } catch (Exception ex) {
                    log.error("Blad podczas tworzenia obiektu", ex);
                    //Domyślnie jest commit - jeśli ustawsz setRollbackOnly transakcja zostanie wycofana
                    status.setRollbackOnly();

                    //Rzucając wyjąytek transakcja rónież zostanie wycofana
                    //throw new RuntimeException("FFF");
                }
            }
        });
    }

    //Uzyty szablon transakcji zwracający wynik
    private Department runInTransactionWithResult(Department department) {
        var template = new TransactionTemplate(transactionManager);
        template.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
        template.setTimeout(30); // 30 sekund

        return template.execute(status -> {
            //Użycie metody z CrudRepository`1
            departmentRepository.save(department);

            //Użycie EntityManager
            //entityManager.persist(department);

            return department;
        });
    }

    //Użyte manualne transakcje - commit/rollback
    private Department runWithManualCommit(Department department) {

        var def = new DefaultTransactionDefinition();
        def.setName("SoftwareSkill");//Ustawienie włąsnej nazwanej transakcji
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            //Użycie metody z CrudRepository
            department = departmentRepository.save(department);

            //Użycie EntityManager
            //entityManager.persist(department);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            throw ex;
        }
        transactionManager.commit(status);
        return department;
    }

    private DepartmentCreationStatus validate(CreateDepartmentDto createDepartmentDto) {
        var validator = new DepartmentValidator(departmentRepository);
        final String departmentName = createDepartmentDto.getName();
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        return DepartmentCreationStatus.IN_PROGRESS;
    }
}
