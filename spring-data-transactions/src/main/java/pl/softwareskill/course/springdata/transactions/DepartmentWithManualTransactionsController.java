package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z tworzeniem departamentów z manualnymi transakcjami
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/manual")
@SuppressWarnings("unused")
public class DepartmentWithManualTransactionsController {

    DepartmentWithManualTransactionFacade facade;

    @PostMapping
    ResponseEntity<?> createDepartment(@RequestBody CreateDepartmentDto createDepartmentDto) {
        DepartmentCreationStatus creationStatus = facade.createDepartment(createDepartmentDto);
        switch (creationStatus) {
            case ALREADY_EXISTS:
                return ResponseEntity.status(HttpStatus.CONFLICT).body("{}");
            case INVALID_INPUT:
                return ResponseEntity.badRequest().body("{}");
            default:
                return ResponseEntity.ok().body("{}");
        }
    }
}
