package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.ManagerDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z operacjami na departamentach bez @Transactional
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/auto")
@SuppressWarnings("unused")
public class DepartmentWithNoTransactionalController {

    private static final String EMPTY_JSON = "{}";

    DepartmentWithNoTransactionalFacade facade;

    @GetMapping("/{departmentId}")
    ResponseEntity<DepartmentDto> findDepartment(@PathVariable("departmentId") Long departmentId) {
        return facade.findDepartment(departmentId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{departmentId}/manager")
    ResponseEntity<ManagerDto> findDepartmentManager(@PathVariable("departmentId") Long departmentId) {
        return facade.getDepartmentManager_ThrowsException(departmentId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{departmentId}")
    public ResponseEntity<?> deleteDepartment(@PathVariable("departmentId") Long departmentId) {
        facade.deleteDepartment(departmentId);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }
}
