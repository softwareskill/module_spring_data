package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.ManagerDto;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class DepartmentWithNoTransactionalFacade {

    DepartmentRepository departmentRepository;

    //Nie jest publiczna, bez anotacji transactional, transakcja i tak będzie
    public Optional<DepartmentDto> findDepartment(Long departmentId) {
        return departmentRepository.findById(departmentId)
                .map(Department::toDto);
    }

    //Nie jest publiczna, bez anotacji transactional, transakcja i tak będzie
    void deleteDepartment(Long departmentId) {
        //departmentRepository.deleteById(departmentId);
        departmentRepository.findById(departmentId)
                .ifPresent(departmentRepository::delete);
    }

    Optional<ManagerDto> getDepartmentManager_ThrowsException(Long departmentId) {
        //Nie ma transakcji, encja zwrócona dla findById jest odpięta od sesji i doczytanie lazy rzuci wyjątek
        return departmentRepository.findById(departmentId)
                .map(Department::getManager)
                .map(Manager::toDto);
    }
}
