package pl.softwareskill.course.springdata.transactions;

class EventPublisher {

    @javax.transaction.Transactional(dontRollbackOn = DepartmentPublishingException.class)
    public void publishDepartmentCreated(String name) {
        if (name.startsWith("Throw")) {
            throw new DepartmentPublishingException("Blad publikacji");
        }
    }
}
