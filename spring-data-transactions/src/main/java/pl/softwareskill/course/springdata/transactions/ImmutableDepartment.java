package pl.softwareskill.course.springdata.transactions;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Immutable;

@Entity
@Table(name = "DEPARTMENTS")
@Data
@AllArgsConstructor
@NoArgsConstructor
// Jeżeli używasz Spring Data to
// - anotacja org.springframework.data.annotation.Immutable nie jest brana pod uwagę
// - jeżeli użyjesz anotacji z Hibernate org.hibernate.annotations.Immutable edycja na poziomie
//   encji zostanie zignorowana a dla query pojawi się WARN w logach lub wyjątek jeśli zostanie ustawiony
//   parametr hibernate.query.immutable_entity_update_query_handling_mode=exception
@Immutable
//@org.hibernate.annotations.Immutable
class ImmutableDepartment {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;
}
