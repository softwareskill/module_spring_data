package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z read only dla SpringData
 * z wykorzystaniem anotacji @Immutable
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/immutable")
@SuppressWarnings("unused")
public class ImmutableDepartmentController {

    private static final String EMPTY_JSON = "{}";

    ImmutableDepartmentFacade facade;

    @PutMapping("/repositoryByEntity/{departmentId}")
    public ResponseEntity<?> updateDepartmentNameByEntityWithRepository(@PathVariable("departmentId") Long departmentId,
                                                                        @RequestParam("name") String name) {
        facade.updateDepartmentNameByEntityWithRepository(departmentId, name);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @PutMapping("/repositoryByQuery/{departmentId}")
    public ResponseEntity<?> updateDepartmentNameByQueryWithRepository(@PathVariable("departmentId") Long departmentId,
                                                                       @RequestParam("name") String name) {
        facade.updateDepartmentNameByQueryWithRepository(departmentId, name);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @PutMapping("/emByEntity/{departmentId}")
    public ResponseEntity<?> updateDepartmentNameByEntityWithEM(@PathVariable("departmentId") Long departmentId, @RequestParam("name") String name) {
        facade.updateDepartmentNameByEntityWithEM(departmentId, name);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }

    @PutMapping("/emByQuery/{departmentId}")
    public ResponseEntity<?> updateDepartmentNameByQueryWithEM(@PathVariable("departmentId") Long departmentId, @RequestParam("name") String name) {
        facade.updateDepartmentNameByQueryWithEM(departmentId, name);
        return ResponseEntity.ok().body(EMPTY_JSON);
    }
}
