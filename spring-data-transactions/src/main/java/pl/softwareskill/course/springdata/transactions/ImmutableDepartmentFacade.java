package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class ImmutableDepartmentFacade {

    ImmutableDepartmentRepository immutableDepartmentRepository;
    EntityManager entityManager;

    @Transactional
    public void updateDepartmentNameByEntityWithRepository(Long departmentId, String newName) {
        //Update nie zostanie wykonany przy flush
        immutableDepartmentRepository.findById(departmentId)
                .ifPresent(immutableDepartment -> immutableDepartment.setName(newName));
    }

    @Transactional
    public void updateDepartmentNameByQueryWithRepository(Long departmentId, String newName) {
        //W logach pojawi się WARN attempts to update an immutable entity,
        // chyba ze zostanie ustawiony parametr hibernate.query.immutable_entity_update_query_handling_mode
        //na exception
        immutableDepartmentRepository.updateDepartmentName(departmentId, newName);
    }

    @Transactional
    public void updateDepartmentNameByEntityWithEM(Long departmentId, String newName) {
        //Update nie zostanie wykonany przy flush
        Optional.ofNullable(entityManager.find(ImmutableDepartment.class, departmentId))
                .ifPresent(immutableDepartment -> immutableDepartment.setName(newName));
    }

    @Transactional
    public void updateDepartmentNameByQueryWithEM(Long departmentId, String newName) {
        var query = entityManager.createQuery("update ImmutableDepartment set name=:name where departmentId=:departmentId");
        query.setParameter(ImmutableDepartment_.NAME, newName);
        query.setParameter(ImmutableDepartment_.DEPARTMENT_ID, departmentId);
        //W logach pojawi się WARN attempts to update an immutable entity
        // chyba ze zostanie ustawiony parametr hibernate.query.immutable_entity_update_query_handling_mode
        //na exception
        var updatedDepartmens = query.executeUpdate();
        log.info("Zaktualizowano {} departamentow", updatedDepartmens);
    }
}
