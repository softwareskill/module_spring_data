package pl.softwareskill.course.springdata.transactions;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ImmutableDepartmentRepository extends JpaRepository<ImmutableDepartment, Long> {

    @Query("update ImmutableDepartment set name=:name where id=:departmentId")
    @Modifying
    void updateDepartmentName(Long departmentId, String name);
}
