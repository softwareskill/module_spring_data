package pl.softwareskill.course.springdata.transactions;

import static java.util.Objects.isNull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.transactions.dto.ManagerDto;

@Entity
@Table(name = "MANAGERS")
@Data
@AllArgsConstructor
@NoArgsConstructor
class Manager {
    @Id
    @Column(name = "MANAGER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MANAGERS")
    Long managerId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    public static ManagerDto toDto(Manager manager) {
        if (isNull(manager)) {
            return null;
        }
        return new ManagerDto(manager.getFirstName(), manager.getLastName());
    }
}
