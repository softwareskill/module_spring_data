package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.QueryHints;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class ReadOnlyDepartmentFacade {

    DepartmentRepository repository;
    EntityManager entityManager;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void updateDepartmentNameByEntityWithRepository(Long departmentId, String newName) {
        repository.findById(departmentId)
                .ifPresent(department -> department.setName(newName));
    }

    //Bez propagation = Propagation.SUPPORTS update się wykona
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void updateDepartmentNameByQueryWithRepository(Long departmentId, String newName) {
        repository.updateDepartmentName(departmentId, newName);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void updateDepartmentNameByEntityWithEM(Long departmentId, String newName) {
        Optional.ofNullable(entityManager.find(Department.class, departmentId))
                .ifPresent(department -> department.setName(newName));
    }

    //Bez propagation = Propagation.SUPPORTS update się wykona
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void updateDepartmentNameByQueryWithEM(Long departmentId, String newName) {
        var query = entityManager.createQuery("update Department set name=:name where departmentId=:departmentId");
        query.setParameter(Department_.NAME, newName);
        query.setParameter(Department_.DEPARTMENT_ID, departmentId);
        var updatedDepartmens = query.executeUpdate();
        log.info("Zaktualizowano {} departamentow", updatedDepartmens);
    }

    //Bez propagation = Propagation.SUPPORTS update się wykona
    @Transactional(propagation = Propagation.SUPPORTS)
    public void updateDepartmentNameByQueryHintsWithEM(Long departmentId, String newName) {
        var query = entityManager.createQuery("update Department set name=:name where departmentId=:departmentId");
        query.setParameter(Department_.NAME, newName);
        query.setParameter(Department_.DEPARTMENT_ID, departmentId);
        query.setHint(QueryHints.HINT_READONLY, true);
        var updatedDepartmens = query.executeUpdate();
        log.info("Zaktualizowano {} departamentow", updatedDepartmens);
    }

    @Transactional
    public void updateDepartmentNameByRepositoryWithoutModifying(Long departmentId, String newName) {
        repository.updateWithoutModifyingDepartmentName(departmentId, newName);
    }
}
