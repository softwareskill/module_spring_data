package pl.softwareskill.course.springdata.transactions;

import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class TransactionHelperService {

    AnotherTransactionalService transactionalService;

    //Jezeli nie będzie @Transactional to bedzie commit
    @Transactional
    public void throwRuntimeException() {
        throw new RuntimeException();
    }

    Optional<String> searchDepartmentName(Long departmentId) {
        return transactionalService.searchDepartmentName(departmentId);
    }

    @Transactional(readOnly = false, timeout = 30)
    public void deleteWithTransactionOverrideAttempt(Long departmentId) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error("Error", e);
        }
        transactionalService.deleteDepartment(departmentId);
    }

    @Transactional(propagation = Propagation.NEVER)
    public DepartmentCreationStatus operationWithPropagationNever() {
        return null;
    }

    @Transactional(propagation = Propagation.NESTED)
    public DepartmentCreationStatus operationWithPropagationNested() {
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DepartmentCreationStatus operationWithPropagationRequiresNew() {
        return null;
    }
}
