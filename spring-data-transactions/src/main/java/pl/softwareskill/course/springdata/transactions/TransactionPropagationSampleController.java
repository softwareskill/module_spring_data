package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

/**
 * Kod demonstracyjny
 * <p>
 * Kontroler z endpointami dla pokazania logiki związanej z propagacją transakcji
 * <p>
 * Aplikacja SpringBoot
 * <p>
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequestMapping(path = "/departments/propagation")
@SuppressWarnings("unused")
public class TransactionPropagationSampleController {

    TransactionPropagationSampleFacade facade;

    @PostMapping("/never")
    ResponseEntity<?> createDepartmentPropagationNever(@RequestBody CreateDepartmentDto createDepartmentDto) {
        facade.createDepartmentPropagationNever(createDepartmentDto);
        return ResponseEntity.ok().body("{}");
    }

    @PostMapping("/neverForChild")
    ResponseEntity<?> createDepartmentChildPropagationNever(@RequestBody CreateDepartmentDto createDepartmentDto) {
        facade.createDepartmentChildPropagationNever(createDepartmentDto);
        return ResponseEntity.ok().body("{}");
    }

    @PostMapping("/mandatory")
    ResponseEntity<?> operateOnMandatoryTransaction() {
        facade.operateOnMandatoryTransaction();
        return ResponseEntity.ok().body("{}");
    }

    @PostMapping("/nested")
    ResponseEntity<?> operateOnNestedTransaction() {
        facade.operateOnNestedTransaction();
        return ResponseEntity.ok().body("{}");
    }

    @PostMapping("/nestedForChild")
    ResponseEntity<?> operateOnNestedChildTransaction() {
        facade.operateOnNestedChildTransaction();
        return ResponseEntity.ok().body("{}");
    }

    @PostMapping("/requiresNewForChild")
    ResponseEntity<?> operateOnRequiresNewChildTransaction() {
        facade.operateOnRequiresNewChildTransaction();
        return ResponseEntity.ok().body("{}");
    }
}
