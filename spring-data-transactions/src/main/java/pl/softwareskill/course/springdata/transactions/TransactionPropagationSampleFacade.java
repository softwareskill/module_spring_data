package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;
import pl.softwareskill.course.springdata.transactions.dto.DepartmentCreationStatus;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Slf4j
class TransactionPropagationSampleFacade {

    DepartmentRepository departmentRepository;
    TransactionHelperService transactionHelperService;

    @Transactional(propagation = Propagation.NEVER)
//    @javax.transaction.Transactional(javax.transaction.Transactional.TxType.NEVER)
    public DepartmentCreationStatus createDepartmentPropagationNever(CreateDepartmentDto createDepartmentDto) {
        //Logika sie powiedzie gdyż nie ma transakcji a bieżąca metoda ją stworzy
        return createAndSaveDepartment(createDepartmentDto);
    }

    @Transactional
    public DepartmentCreationStatus createDepartmentChildPropagationNever(CreateDepartmentDto createDepartmentDto) {
        //Logika sie nie powiedzie gdyż transakcja zostanie utworzona w ramach tej metody
        //a transactionHelperService rzuci już wyjątek
        return transactionHelperService.operationWithPropagationNever();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public void operateOnMandatoryTransaction() {
        //Logika sie nie powiedzie gdyż jest wymagane aby transakcja była wcześniej rozpoczęta
        log.info("operateOnMandatoryTransaction");
    }

    @Transactional(propagation = Propagation.NESTED)
    public void operateOnNestedTransaction() {
        //Logika sie powiedzie gdyż nie ma wcześniejszej transakcji (nie ma zagnieżdżenia)
        log.info("operateOnNestedTransaction");
    }

    @Transactional
    public void operateOnNestedChildTransaction() {
        //Logika sie nie powiedzie gdyż domyślnie EM nie wspiera tego typu transakcji
        transactionHelperService.operationWithPropagationNested();
    }

    @Transactional
    public void operateOnRequiresNewChildTransaction() {
        transactionHelperService.operationWithPropagationRequiresNew();
    }

    private DepartmentCreationStatus createAndSaveDepartment(CreateDepartmentDto createDepartmentDto) {
        final String departmentName = createDepartmentDto.getName();
        log.info("Tworzenie departamentu {}", departmentName);
        DepartmentCreationStatus processingStatus = validate(createDepartmentDto);
        if (!DepartmentCreationStatus.IN_PROGRESS.equals(processingStatus)) {
            return processingStatus;
        }
        var department = Department.fromDto(createDepartmentDto);

        departmentRepository.save(department);

        return DepartmentCreationStatus.CREATED;
    }

    private DepartmentCreationStatus validate(CreateDepartmentDto createDepartmentDto) {
        var validator = new DepartmentValidator(departmentRepository);
        final String departmentName = createDepartmentDto.getName();
        if (!validator.isRequestValid(createDepartmentDto)) {
            log.error("Nieprawidlowe dane wejsciowe dla tworzenia nowego depatrtamentu {}", createDepartmentDto);
            return DepartmentCreationStatus.INVALID_INPUT;
        }
        if (!validator.departmentDoesntExist(departmentName)) {
            log.error("Departament {} juz istnieje", departmentName);
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        return DepartmentCreationStatus.IN_PROGRESS;
    }
}
