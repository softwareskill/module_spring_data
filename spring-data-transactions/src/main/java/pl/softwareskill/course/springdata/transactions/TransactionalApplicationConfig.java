package pl.softwareskill.course.springdata.transactions;

import javax.persistence.EntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@SuppressWarnings("unused")
class TransactionalApplicationConfig {

    @Bean
    DepartmentWithManualTransactionFacade departmentWithManualTransactionFacade(DepartmentRepository departmentRepository,
                                                                                EntityManager entityManager,
                                                                                PlatformTransactionManager transactionManager) {
        return new DepartmentWithManualTransactionFacade(departmentRepository, entityManager, transactionManager);
    }

    @Bean
    AnotherTransactionalService anotherTransactionalService(DepartmentRepository departmentRepository) {
        return new AnotherTransactionalService(departmentRepository);
    }

    @Bean
    TransactionHelperService transactionHelperService(AnotherTransactionalService anotherTransactionalService) {
        return new TransactionHelperService(anotherTransactionalService);
    }

    @Bean
    DepartmentWithAutoTransactionFacade departmentWithAutoTransactionFacade(DepartmentRepository departmentRepository,
                                                                            EntityManager entityManager,
                                                                            TransactionHelperService transactionHelperService) {
        return new DepartmentWithAutoTransactionFacade(departmentRepository, entityManager,
                transactionHelperService);
    }

    @Bean
    DepartmentWithNoTransactionalFacade departmentWithNoTransactionalFacade(DepartmentRepository departmentRepository) {
        return new DepartmentWithNoTransactionalFacade(departmentRepository);
    }

    @Bean
    EventPublisher eventPublisher() {
        return new EventPublisher();
    }

    @Bean
    DepartmentTransactionalExceptionHandlingFacade departmentTransactionalExceptionHandlingFacade(DepartmentRepository departmentRepository,
                                                                                                  EventPublisher publisher,
                                                                                                  TransactionHelperService transactionHelperService) {
        return new DepartmentTransactionalExceptionHandlingFacade(departmentRepository, publisher, transactionHelperService);
    }

    @Bean
    ImmutableDepartmentFacade departmentWithReadOnlyFacade(ImmutableDepartmentRepository immutableDepartmentRepository, EntityManager entityManager) {
        return new ImmutableDepartmentFacade(immutableDepartmentRepository, entityManager);
    }

    @Bean
    ReadOnlyDepartmentFacade readOnlyDepartmentFacade(DepartmentRepository departmentRepository, EntityManager entityManager) {
        return new ReadOnlyDepartmentFacade(departmentRepository, entityManager);
    }

    @Bean
    TransactionPropagationSampleFacade transactionPropagationSampleFacade(DepartmentRepository departmentRepository,
                                                                          TransactionHelperService transactionHelperService) {
        return new TransactionPropagationSampleFacade(departmentRepository, transactionHelperService);
    }
}
