package pl.softwareskill.course.springdata.transactions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = TransactionalDemoApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public abstract class AbstractDepartmentIntegrationTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    SampleDepartmentData sampleDepartmentData;
}
