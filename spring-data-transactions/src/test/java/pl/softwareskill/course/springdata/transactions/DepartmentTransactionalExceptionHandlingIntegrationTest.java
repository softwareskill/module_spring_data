package pl.softwareskill.course.springdata.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleAddress;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

public class DepartmentTransactionalExceptionHandlingIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldNotCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/exceptions")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name("Throw" + System.nanoTime())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/exceptions")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
