package pl.softwareskill.course.springdata.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleAddress;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleUniqueDepartmentName;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

public class DepartmentWithAutoTransactionIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/auto")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotCreateNonUniqueDepartment() throws Exception {
        String existingDepartmentName = "Hibernate" + System.nanoTime();

        existingDepartment(existingDepartmentName);

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments/auto")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    private void existingDepartment(String existingDepartmentName) throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments/auto")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .build();
        mvc.perform(post("/departments/auto")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldFindDepartment() throws Exception {
        Long existingDepartmentId = 1L;
        mvc.perform(get("/departments/auto/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldDeleteDepartment() throws Exception {
        Long existingDepartmentId = 2L;
        mvc.perform(delete("/departments/auto/{departmentId}", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldCallDeleteDepartmentWithReadOnly() throws Exception {
        Long existingDepartmentId = 2L;
        mvc.perform(delete("/departments/auto/{departmentId}/readOnly", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldCallFindDepartmentWithTimeout() throws Exception {
        Long existingDepartmentId = 2L;
        mvc.perform(get("/departments/auto/{departmentId}/timeout", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldCallDeleteDepartmentWithTimeout() throws Exception {
        Long existingDepartmentId = 2L;
        mvc.perform(delete("/departments/auto/{departmentId}/timeout", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    //Transakcja jest propagowana w dół, niezaleznie od tego czy po drodze jest jakaś nietransakcyjna metoda
    @Test
    void shouldFindDepartmentName() throws Exception {
        Long existingDepartmentId = 1L;
        mvc.perform(get("/departments/auto/{departmentId}/name", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
