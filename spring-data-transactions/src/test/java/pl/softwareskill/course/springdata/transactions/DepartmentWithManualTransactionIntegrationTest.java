package pl.softwareskill.course.springdata.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleAddress;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleUniqueDepartmentName;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

public class DepartmentWithManualTransactionIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldCreateDepartment() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/manual")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotCreateNonUniqueDepartment() throws Exception {
        String existingDepartmentName = "Hibernate" + System.nanoTime();

        existingDepartment(existingDepartmentName);

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments/manual")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    private void existingDepartment(String existingDepartmentName) throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(existingDepartmentName)
                .address(sampleAddress())
                .build();
        mvc.perform(post("/departments/manual")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotCreateDepartmentWhenDataIsInvalid() throws Exception {
        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .build();
        mvc.perform(post("/departments/manual")
                .content(new ObjectMapper().writeValueAsString(createDepartmentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
