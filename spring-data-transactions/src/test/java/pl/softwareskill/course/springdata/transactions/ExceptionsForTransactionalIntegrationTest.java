package pl.softwareskill.course.springdata.transactions;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ExceptionsForTransactionalIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldThrowExceptionOnFindDepartmentManager() throws Exception {
        Long existingDepartmentId = 1L;
        mvc.perform(get("/departments/auto/{departmentId}/manager", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    //RuntimeException w transakcyjnej metodzie spowoduje ze transakcja zostanie na końcu wycofana, niezależnie
    //od tego, czy wyjątek został przechwycony
    @Test
    void shouldDeleteDepartmentWithCatchedException() throws Exception {
        Long existingDepartmentId = 3L;
        mvc.perform(delete("/departments/exceptions/{departmentId}/withCatchedException", existingDepartmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }
}
