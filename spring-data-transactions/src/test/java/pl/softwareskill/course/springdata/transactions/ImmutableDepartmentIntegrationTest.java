package pl.softwareskill.course.springdata.transactions;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ImmutableDepartmentIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Autowired
    SampleDepartmentData sampleDepartmentData;

    @Test
    void shouldUpdateDepartmentNameByEntityWithRepository() throws Exception {
        final String departmentName = "Unique " + System.currentTimeMillis();
        Long departmentId = sampleDepartmentData.createAndSaveDepartment(departmentName);

        final String newDepartmentName = "NEW_Unique" + System.currentTimeMillis();

        mvc.perform(put("/departments/immutable/repositoryByEntity/{departmenId}?name={name}", departmentId, newDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdateDepartmentNameByQueryWithRepository() throws Exception {
        final String departmentName = "Unique " + System.currentTimeMillis();
        Long departmentId = sampleDepartmentData.createAndSaveDepartment(departmentName);

        final String newDepartmentName = "NEW_Unique" + System.currentTimeMillis();

        mvc.perform(put("/departments/immutable/repositoryByQuery/{departmenId}?name={name}", departmentId, newDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdateDepartmentNameByEntityWithEM() throws Exception {
        final String departmentName = "Unique " + System.currentTimeMillis();
        Long departmentId = sampleDepartmentData.createAndSaveDepartment(departmentName);

        final String newDepartmentName = "NEW_Unique" + System.currentTimeMillis();

        mvc.perform(put("/departments/immutable/emByEntity/{departmenId}?name={name}", departmentId, newDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdateDepartmentNameByQueryWithEM() throws Exception {
        final String departmentName = "Unique " + System.currentTimeMillis();
        Long departmentId = sampleDepartmentData.createAndSaveDepartment(departmentName);

        final String newDepartmentName = "NEW_Unique" + System.currentTimeMillis();

        mvc.perform(put("/departments/immutable/emByQuery/{departmenId}?name={name}", departmentId, newDepartmentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
