package pl.softwareskill.course.springdata.transactions;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.springdata.transactions.dto.AddressDto;

@Component
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Transactional
class SampleDepartmentData {

    DepartmentRepository repository;

    @Transactional
    public Long createAndSaveDepartment(String departmentName) {
        var department = new Department();
        department.setName(departmentName);
        department.setAddress(Address.fromDto(sampleAddress()));
        department = repository.save(department);
        return department.getDepartmentId();
    }


    static AddressDto sampleAddress() {
        return AddressDto.builder()
                .street("street")
                .country("PL")
                .postalCode("41-900")
                .city("Katowice")
                .streetNumber("11A")
                .build();
    }

    static String sampleUniqueDepartmentName() {
        return "unique" + System.currentTimeMillis();
    }
}
