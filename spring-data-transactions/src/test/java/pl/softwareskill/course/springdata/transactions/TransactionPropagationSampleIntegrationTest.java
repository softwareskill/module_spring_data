package pl.softwareskill.course.springdata.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleAddress;
import static pl.softwareskill.course.springdata.transactions.SampleDepartmentData.sampleUniqueDepartmentName;
import pl.softwareskill.course.springdata.transactions.dto.CreateDepartmentDto;

public class TransactionPropagationSampleIntegrationTest extends AbstractDepartmentIntegrationTest {

    @Test
    void shouldCreateDepartmentWithPropagationNever() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/propagation/never")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnErrorOnCreateDepartmentWithChildPropagationNever() throws Exception {

        CreateDepartmentDto createDepartmentDto = CreateDepartmentDto.builder()
                .name(sampleUniqueDepartmentName())
                .address(sampleAddress())
                .build();

        String contentAsJson = new ObjectMapper().writeValueAsString(createDepartmentDto);

        mvc.perform(post("/departments/propagation/neverForChild")
                .content(contentAsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldReturnErrorOnPropagationMandatory() throws Exception {

        mvc.perform(post("/departments/propagation/mandatory")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldReturnErrorOnPropagationNested() throws Exception {

        mvc.perform(post("/departments/propagation/nested")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnErrorOnPropagationNestedForChild() throws Exception {

        mvc.perform(post("/departments/propagation/nestedForChild")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void shouldReturnErrorOnPropagationRequiresNewForChild() throws Exception {

        mvc.perform(post("/departments/propagation/requiresNewForChild")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
